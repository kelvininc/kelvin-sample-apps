{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Process Plant Example - Building From Scratch\n",
    "The previous notebook gave a general overview of the Process Plant usecase. Since app names and versions uploaded to the Kelvin <a href=\"https://documentation.kelvininc.com/platform/control_center/app_registry/\">App Registry</a> must be unique, you can't upload and deploy the \"process-plant-control\" application as is. This module covers the steps of creating a new app just like the one in the previous module."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "1. Narrative\n",
    "2. Prerequisites\n",
    "3. Building Kelvin SDK Applications\n",
    "4. App Emulation\n",
    "5. Interrogating OPC-UA Data\n",
    "6. Simulator UI\n",
    "7. Utilizing Local OPC-UA History\n",
    "8. Shutting Down Apps\n",
    "9. Next Steps\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Narrative\n",
    "The demonstration process will simulate the inflow and outflow of a holding vessel (H1 ). The Control Valve V1 opens based upon the downstream demand of the process.  A Pump (P1) maintains level in Vessel V1 by varying the inflow to the vessel based upon the demand caused by the open position of Valve (V1) (monitoring the level via the Level transmitter LT1).\n",
    "\n",
    "![PNID](images/ProcessPlantPNIDmed.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Problem\n",
    "\n",
    "The Holding Vessel Process was conceived with an anticipated inflow and outflow range. Due to external conditions the pump P1 must adapt to the demand from the outlet valve V1. The example in this notebook utilizes simple pump start and stop setpoints, the idea is to demonstrate control, then move on to a more complex control algorithm later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Prerequisites"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Verify KSDK version\n",
    "This usecase requires KSDK version 3.0 or greater."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk --version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Log Into the Kelvin Platform\n",
    "To work with the KSDK you must be logged into a Kelvin Platform. \n",
    "Either log in from a terminal window or use the cells below.\n",
    "\n",
    "```ksdk login <yourinstance>.kelvininc.com ```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "username=input()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import getpass\n",
    "userpassword = getpass.getpass()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Log into your KSDK server, you can do this from the terminal\n",
    "! ksdk login titan.kelvininc.com --username {username} --password {userpassword}\n",
    "userpassword = ''"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Building Kelvin SDK Applications\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating Applications\n",
    "The Kelvin SDK control app will connect to the PLC/Simulator, perform logic, then write control changes back out to the PLC/Simulator.\n",
    "\n",
    "To create the app, type ```ksdk app create <appname>```. Pick a unique name for the app, it is suggested that you use your own name to differentiate the app in the system i.e. john-smith-plant-control "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk app create test-plant-control"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A new folder will be created with the app name, all the necessary files will be created."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Python Program\n",
    "Navigate to locate the main program, it will be located in a subfolder with the same name.\n",
    "    \n",
    "![OPC-UA Commander](images/process-plant-control-program.jpg)    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is the entire process plant control application, copy it into your own python program. We will break down the code in the next section. \n",
    "```python\n",
    "from kelvin.core.application import DataModelApplication\n",
    "from kelvin.message.raw import Float32\n",
    "\n",
    "class App(DataModelApplication):\n",
    "\n",
    "    def on_initialize(self, configuration: dict) -> bool:\n",
    "        #print(configuration)\n",
    "        vessel_level = 0\n",
    "        self.pump_speed_setpoint = 0\n",
    "        self.pump_speed_feedback = 0\n",
    "        self.valve_position_setpoint = 0\n",
    "        self.valve_position_feedback = 0                  \n",
    "        self.meter_1_flowrate = 0\n",
    "        self.meter_2_flowrate = 0  \n",
    "        self.out_speed_setpoint = 0\n",
    "        \n",
    "        self.vessel_high_setpoint = configuration[\"thresholds\"][\"vessel_high_setpoint\"][\"value\"]\n",
    "        self.vessel_low_setpoint = configuration[\"thresholds\"][\"vessel_low_setpoint\"][\"value\"]\n",
    "        print('###### Initialized ######')\n",
    "        print('vessel_low_setpoint:' + str(self.vessel_low_setpoint) + '    vessel_high_setpoint:' + str(self.vessel_high_setpoint))\n",
    "        return True\n",
    "\n",
    "    def write_out(self, item, data) -> None:\n",
    "        f = Float32(item)\n",
    "        f.value = data\n",
    "        self.emit(f)\n",
    "        #print(\"emitting - \" + item + \":\" + str(data))   \n",
    "\n",
    "    def on_data(self, data) -> None:\n",
    "        vessel_level = 0\n",
    "        #print(data)\n",
    "        for item in data:\n",
    "            #print(item)\n",
    "            message_name = item.name\n",
    "            #print message_name\n",
    "            if message_name == \"vessel_level\":\n",
    "                self.vessel_level = item.value\n",
    "            elif message_name == \"pump_speed_setpoint\":\n",
    "                self.pump_speed_setpoint = item.value\n",
    "            elif message_name == \"pump_speed_feedback\":\n",
    "                self.pump_speed_feedback = item.value\n",
    "            elif message_name == \"valve_position_setpoint\":\n",
    "                self.valve_position_setpoint = item.value\n",
    "            elif message_name == \"valve_position_feedback\":\n",
    "                self.valve_position_feedback = item.value                   \n",
    "            elif message_name == \"meter_1_flowrate\":\n",
    "                self.meter_1_flowrate = item.value\n",
    "            elif message_name == \"meter_2_flowrate\":\n",
    "                self.meter_2_flowrate = item.value \n",
    "        \n",
    "        if self.vessel_level < self.vessel_low_setpoint:\n",
    "            self.out_speed_setpoint = 60\n",
    "        elif self.vessel_level > self.vessel_high_setpoint:\n",
    "            self.out_speed_setpoint = 0\n",
    "            \n",
    "        sp = Float32(\"speed_setpoint\")\n",
    "        sp.value = self.out_speed_setpoint\n",
    "        \n",
    "        vpos = Float32(\"valve_position\")\n",
    "        vpos.value = self.valve_position_feedback\n",
    "        vpos.time_of_validity = sp.time_of_validity\n",
    "        \n",
    "        tl = Float32(\"tank_level\")\n",
    "        tl.value = self.vessel_level        \n",
    "        tl.time_of_validity = sp.time_of_validity     \n",
    "        \n",
    "        self.emit(sp)        \n",
    "        self.emit(vpos)          \n",
    "        self.emit(tl)          \n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Imports\n",
    "At the top of the program are program imports. The only data type we are using is the kelvin.message.raw.Float32. There are other raw data models available. You may also create your own complex custom data models which is covered in a different notebook.\n",
    "\n",
    "```python\n",
    "from kelvin.core.application import DataModelApplication\n",
    "from kelvin.message.raw import Float32\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### on_initialize\n",
    "```on_nitialize``` fires fit first time the application starts. We are defining global variables, indicated by ```self.<variablename>```. This will allow the variables to be retained and accessed by any subroutine. We also print a Initialized message to the console.\n",
    "\n",
    "```python\n",
    "    def on_initialize(self, configuration: dict) -> bool:\n",
    "        #print(configuration)\n",
    "        vessel_level = 0\n",
    "        self.pump_speed_setpoint = 0\n",
    "        self.pump_speed_feedback = 0\n",
    "        self.valve_position_setpoint = 0\n",
    "        self.valve_position_feedback = 0                  \n",
    "        self.meter_1_flowrate = 0\n",
    "        self.meter_2_flowrate = 0  \n",
    "        self.out_speed_setpoint = 0\n",
    "        \n",
    "        self.vessel_high_setpoint = configuration[\"thresholds\"][\"vessel_high_setpoint\"][\"value\"]\n",
    "        self.vessel_low_setpoint = configuration[\"thresholds\"][\"vessel_low_setpoint\"][\"value\"]\n",
    "        print('###### Initialized ######')\n",
    "        print('vessel_low_setpoint:' + str(self.vessel_low_setpoint) + '    vessel_high_setpoint:' + str(self.vessel_high_setpoint))\n",
    "        return True\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The ```write_out``` subroutine is a helper to emit data back to the OPC-UA data buss.\n",
    "\n",
    "```python\n",
    "    def write_out(self, item, data) -> None:\n",
    "        f = Float32(item)\n",
    "        f.value = data\n",
    "        self.emit(f)\n",
    "        #print(\"emitting - \" + item + \":\" + str(data))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### on_data\n",
    "```on_data``` executes each time data is received by the application. The app starts by assigning data received out of the ```data``` object to the variables. Then, a simple calculation is performed to turn on or off the pump. Finally, the app emits three values back out to the OPC-UA data buss, it specifically synchs up the timestamps of all three values to simplify using the data later.\n",
    "\n",
    "```python\n",
    "    def on_data(self, data) -> None:\n",
    "        vessel_level = 0\n",
    "        #print(data)\n",
    "        for item in data:\n",
    "            #print(item)\n",
    "            message_name = item.name\n",
    "            #print message_name\n",
    "            if message_name == \"vessel_level\":\n",
    "                self.vessel_level = item.value\n",
    "            elif message_name == \"pump_speed_setpoint\":\n",
    "                self.pump_speed_setpoint = item.value\n",
    "            elif message_name == \"pump_speed_feedback\":\n",
    "                self.pump_speed_feedback = item.value\n",
    "            elif message_name == \"valve_position_setpoint\":\n",
    "                self.valve_position_setpoint = item.value\n",
    "            elif message_name == \"valve_position_feedback\":\n",
    "                self.valve_position_feedback = item.value                   \n",
    "            elif message_name == \"meter_1_flowrate\":\n",
    "                self.meter_1_flowrate = item.value\n",
    "            elif message_name == \"meter_2_flowrate\":\n",
    "                self.meter_2_flowrate = item.value \n",
    "        \n",
    "        if self.vessel_level < self.vessel_low_setpoint:\n",
    "            self.out_speed_setpoint = 60\n",
    "        elif self.vessel_level > self.vessel_high_setpoint:\n",
    "            self.out_speed_setpoint = 0\n",
    "            \n",
    "        sp = Float32(\"speed_setpoint\")\n",
    "        sp.value = self.out_speed_setpoint\n",
    "        \n",
    "        vpos = Float32(\"valve_position\")\n",
    "        vpos.value = self.valve_position_feedback\n",
    "        vpos.time_of_validity = sp.time_of_validity\n",
    "        \n",
    "        tl = Float32(\"tank_level\")\n",
    "        tl.value = self.vessel_level        \n",
    "        tl.time_of_validity = sp.time_of_validity     \n",
    "        \n",
    "        self.emit(sp)        \n",
    "        self.emit(vpos)          \n",
    "        self.emit(tl)          \n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## app.yaml\n",
    "The app.yaml file defines what version of KSDK and base image to use when building the app, it's also where you define application inputs and outputs. All data values are defined as being Float32 datatypes.\n",
    "\n",
    "Replace your application's app.yaml content with the code below from the \"configuration:\" section down.\n",
    "\n",
    "##### Note: Do not copy the entire app.yaml code below otherwise you must modify the \"name\", \"title\", and \"entry_point\" items in the \"info:\" section to reflect your application.\n",
    "\n",
    "```yaml\n",
    "sdk:\n",
    "  sdk_version: 3.0.1\n",
    "  base_image: titan.kelvininc.com:5000/kelvin-core-python:2.1.1\n",
    "\n",
    "info:\n",
    "  name: process-plant-control\n",
    "  title: process-plant-control\n",
    "  description: Process Plant Control App.\n",
    "  version: 0.0.1\n",
    "  platform: python\n",
    "  type: data_model\n",
    "  entry_point: process_plant_control/process_plant_control.py\n",
    "\n",
    "datamodels:\n",
    "  - http://kelvininc.com/raw/float32:1.0.0\n",
    "\n",
    "capabilities:\n",
    "\n",
    "resources:\n",
    "  memory: 128M\n",
    "  cpu: 0.1\n",
    "\n",
    "configuration:\n",
    "  descriptor:\n",
    "    type: poller\n",
    "    period: 5.0\n",
    "  thresholds:\n",
    "    vessel_high_setpoint:\n",
    "      title: Vessel high setpoint\n",
    "      value: 70\n",
    "      type: float\n",
    "      units: IN\n",
    "      default: 70\n",
    "    vessel_low_setpoint:\n",
    "      title: Vessel low setpoint\n",
    "      value: 50\n",
    "      type: int\n",
    "      units: IN\n",
    "      default: 50\n",
    "\n",
    "inputs:\n",
    "  vessel_level:\n",
    "    type: raw.float32\n",
    "  pump_speed_setpoint:\n",
    "    type: raw.float32\n",
    "  pump_speed_feedback:\n",
    "    type: raw.float32\n",
    "  valve_position_setpoint:\n",
    "    type: raw.float32\n",
    "  valve_position_feedback:\n",
    "    type: raw.float32\n",
    "  meter_1_flowrate:\n",
    "    type: raw.float32\n",
    "  meter_2_flowrate:\n",
    "    type: raw.float32\n",
    "\n",
    "outputs:\n",
    "  speed_setpoint:\n",
    "    type: raw.float32\n",
    "  valve_position:\n",
    "    type: raw.float32\n",
    "  tank_level:\n",
    "    type: raw.float32    \n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## environment.yaml\n",
    "The environment.yaml file defines input and output bindings for the app's OPC-UA server's tags.\n",
    "\n",
    "Replace your application's app.yaml content with the code below.\n",
    "\n",
    "##### Note: You may want to customize your DCN Name to help access the data later.\n",
    "\n",
    "```yaml\n",
    "info:\n",
    "  name: process-plant-dcn\n",
    "  title: Process Plant DCN\n",
    "  description: DCN for the Process Plant use case.\n",
    "  version: 1.0.0\n",
    "  logging_level: INFO\n",
    "   \n",
    "apps:\n",
    "  process-plant-control:\n",
    "    source: .\n",
    "    config: app.yaml\n",
    "\n",
    "bindings:\n",
    "  inputs:\n",
    "    - endpoint: opc.tcp://process-plant-simulator:53880\n",
    "      source: ns=2;s=vessel_level\n",
    "      target: vessel_level\n",
    "      config:\n",
    "        external_tag: ns=1;s=Vessel-001/Level\n",
    "    - endpoint: opc.tcp://process-plant-simulator:53880\n",
    "      source: ns=2;s=pump_speed_setpoint\n",
    "      target: pump_speed_setpoint\n",
    "      config:\n",
    "        external_tag: ns=1;s=Pump-001/SpeedSetpoint        \n",
    "    - endpoint: opc.tcp://process-plant-simulator:53880\n",
    "      source: ns=2;s=pump_speed_feedback\n",
    "      target: pump_speed_feedback\n",
    "      config:\n",
    "        external_tag: ns=1;s=Pump-001/SpeedFeedback\n",
    "    - endpoint: opc.tcp://process-plant-simulator:53880\n",
    "      source: ns=2;s=valve_position_setpoint\n",
    "      target: valve_position_setpoint\n",
    "      config:\n",
    "        external_tag: ns=1;s=Valve-001/PositionSetpoint       \n",
    "    - endpoint: opc.tcp://process-plant-simulator:53880\n",
    "      source: ns=2;s=valve_position_feedback\n",
    "      target: valve_position_feedback\n",
    "      config:\n",
    "        external_tag: ns=1;s=Valve-001/PositionFeedback        \n",
    "    - endpoint: opc.tcp://process-plant-simulator:53880\n",
    "      source: ns=2;s=meter_1_flowrate\n",
    "      target: meter_1_flowrate\n",
    "      config:\n",
    "        external_tag: ns=1;s=Meter-001/Flowrate        \n",
    "    - endpoint: opc.tcp://process-plant-simulator:53880\n",
    "      source: ns=2;s=meter_2_flowrate\n",
    "      target: meter_2_flowrate\n",
    "      config:\n",
    "        external_tag: ns=1;s=Meter-002/Flowrate    \n",
    "    \n",
    "  outputs:\n",
    "    - endpoint: opc.tcp://default_app:48010\n",
    "      source: speed_setpoint\n",
    "      target: ns=2;s=speed_setpoint\n",
    "      access: RW\n",
    "    - endpoint: opc.tcp://default_app:48010\n",
    "      source: valve_position\n",
    "      target: ns=2;s=valve_position\n",
    "      access: RW\n",
    "    - endpoint: opc.tcp://default_app:48010\n",
    "      source: tank_level\n",
    "      target: ns=2;s=tank_level\n",
    "      access: RW\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building  Apps\n",
    "Applications are built using the ```ksdk app build``` command. Append ```--verbose``` to see more detailed build output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build the control application\n",
    "! ksdk app build --app-dir=test-plant-control"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Build the plant simulator (the first build will take some time and have a lot of output!)\n",
    "! docker build -f process-plant-simulator/Dockerfile . -t process-plant-simulator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. App Emulation\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Emulation\n",
    "<a href=\"https://docs.kelvininc.com/sdk/apps/emulating_apps/prepare_an_app_for_emulation/\">KSDK emulation</a> enables running apps immediately for testing. ```ksdk emulation start``` will start the app found in the immediate directory of the command prompt or Jupyter notebook. You may also start apps from anywhere by name, with ```ksdk emulation start <app-image-name:version>```. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start the application\n",
    "! (cd process-plant-control && ksdk emulation start --port-mapping 48010:48010)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start the plant simulator \n",
    "! docker run --rm -d -p 1880:1880 -p 53880:53880 --network ksdk --name process-plant-simulator process-plant-simulator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Images and Containers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check kelvin app images and running applications, note, the simulator is not a KSDK app but a simple docker container\n",
    "! ksdk app images list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Standard Docker commands work as well, this shows running containers\n",
    "! docker ps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the logs of the application for any errors, press the stop icon above to interrupt the log\n",
    "# This may work better in a terminal window\n",
    "! ksdk emulation logs process-plant-control:0.0.3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You can also check the logs of the data injector, press the stop icon above to interrupt the log\n",
    "# This may work better in a terminal\n",
    "! docker logs process-plant-simulator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Interrogating OPC-UA Data\n",
    "## OPC-UA Commander\n",
    "With OPC-UA Commander you can view the OPC-UA data in your app to verify its operation. \n",
    "Use the arrow keys to navigate into the Objects group.\n",
    "Press M when a tag is highlighted to monitor its live values. Then press Q to quit the program.\n",
    "\n",
    "![OPC-UA Commander](images/opcuacommander.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You can inspect the OPCUA server with an OPCUA client such as OPCUA Commander, this command will install it\n",
    "# https://github.com/node-opcua/opcua-commander\n",
    "# You only need to install the OPCUA Commander once after your VM is created\n",
    "! npm -g install opcua-commander xml-writer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note, OPCUA Commander needs to be executed in a terminal window \n",
    "opcua-commander -e opc.tcp://$(docker port process-plant-control 48010)\n",
    "opcua-commander -e opc.tcp://localhost:53880    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6. Simulator UI\n",
    "### Node-RED User Interface\n",
    "The Node-RED simulator provides a built-in web user interface to view the simulators live data.\n",
    "\n",
    "If developing on a local machine simply navigate with a web browser to http://localhost:1880/ui"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Launch the Node-RED dashboard user interface webpage from within the Kelvin Sandbox\n",
    "# you may need to modify the URL sub domain name\n",
    "import os, re\n",
    "from IPython.display import HTML\n",
    "user_name = os.environ[\"JUPYTERHUB_USER\"]\n",
    "height = 620\n",
    "#url = f\"https://sandbox.kelvininc.com/user/{user_name}/proxy/1880/ui/\"\n",
    "url = f\"https://litterbox.kelvininc.com/user/{user_name}/proxy/1880/ui/\"\n",
    "print(url)\n",
    "display(HTML(f\"\"\"<iframe src=\"{url}\" width=\"1000px\" height=\"{height}px\"/>\"\"\" ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may modify the pump speed and valve position setpoints as well as switch the valves into manual/auto modes.\n",
    "\n",
    "When the pump is set to manual, control is coming from the KSDK app to control the pump speed.\n",
    "\n",
    "![PNID](images/ProcessPlantPNIDmed.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 7. Utilizing Local OPC-UA History\n",
    "Kelvin aplications use OPC-UA for default between app communications. You can can utalize python OPC-UA libraries such as <a href=\"https://github.com/FreeOpcUa/opcua-asyncio\">opcua-asyncio</a> to collect live data and history from any OPC-UA server. We will also use <a href=\"https://pandas.pydata.org/\">pandas</a> to manipulate our data and <a href=\"https://matplotlib.org/\">matplotlib</a> to display a plot in the Jupyter Notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#! pip install asyncua"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import libraries into the notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import asyncua\n",
    "import pandas\n",
    "from datetime import datetime, timedelta, timezone\n",
    "import matplotlib.pyplot as pyplot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instantiate an asyncua client, pull OPC-UA history and prepare it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ua_client = asyncua.Client(\"opc.tcp://localhost:48010\")\n",
    "await ua_client.connect()\n",
    "\n",
    "now = datetime.now(tz=timezone.utc)\n",
    "then = now - timedelta(minutes=5)\n",
    "\n",
    "node = ua_client.get_node(\"ns=2;s=tank_level\")\n",
    "data = await node.read_raw_history(starttime=then, endtime=now, numvalues=10000)\n",
    "def convert(x):\n",
    "    return {**{k: v for k, v in vars(x).items() if not k.startswith(\"_\")}, \"Value\": x.Value.Value}\n",
    "data = pandas.DataFrame([convert(v) for v in data])\n",
    "data = data.drop(columns=['Encoding', 'SourcePicoseconds', 'ServerPicoseconds', 'ServerTimestamp', 'StatusCode'])\n",
    "tankLevel = data.rename(columns={'Value':'TankLevel', 'SourceTimestamp':'Timestamp'})\n",
    "\n",
    "node2 = ua_client.get_node(\"ns=2;s=valve_position\")\n",
    "data = await node2.read_raw_history(starttime=then, endtime=now, numvalues=10000)\n",
    "def convert(x):\n",
    "    return {**{k: v for k, v in vars(x).items() if not k.startswith(\"_\")}, \"Value\": x.Value.Value}\n",
    "data = pandas.DataFrame([convert(v) for v in data])\n",
    "data = data.drop(columns=['Encoding', 'SourcePicoseconds', 'ServerPicoseconds', 'ServerTimestamp', 'StatusCode'])\n",
    "valvePosition = data.rename(columns={'Value':'ValvePosition', 'SourceTimestamp':'vts'})\n",
    "\n",
    "node2 = ua_client.get_node(\"ns=2;s=speed_setpoint\")\n",
    "data = await node2.read_raw_history(starttime=then, endtime=now, numvalues=10000)\n",
    "def convert(x):\n",
    "    return {**{k: v for k, v in vars(x).items() if not k.startswith(\"_\")}, \"Value\": x.Value.Value}\n",
    "data = pandas.DataFrame([convert(v) for v in data])\n",
    "data = data.drop(columns=['Encoding', 'SourcePicoseconds', 'ServerPicoseconds', 'ServerTimestamp', 'StatusCode'])\n",
    "speedSetpoint = data.rename(columns={'Value':'SpeedSetpoint', 'SourceTimestamp':'sts'})\n",
    "\n",
    "result = pandas.concat([tankLevel, valvePosition, speedSetpoint], axis=1, join='inner')\n",
    "df = result.drop(columns=['vts', 'sts'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyplot.figure(figsize=(15,8)) \n",
    "pyplot.plot(df.Timestamp, df.TankLevel, label=\"Tank Level\" )\n",
    "pyplot.plot(df.Timestamp, df.ValvePosition, label=\"Outlet Valve Position\" )\n",
    "pyplot.plot(df.Timestamp, df.SpeedSetpoint, label=\"Pump Speed\" )\n",
    "pyplot.legend(bbox_to_anchor=(0, .09, .3, 1), loc='upper left', ncol=2, mode=\"sdf\", borderaxespad=0.)\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 8. Shutting Down Apps\n",
    "The typical way to stop an application you are working on is to type ```ksdk emulation stop``` from the terminal while in the app directory. \n",
    "\n",
    "To stop apps from anywhere add the ```<app-name:version>``` argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk emulation stop process-plant-control:0.0.3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```ksdk emulation stop``` may delete the docker 'ksdk' network which may in turn terminate the simulator, if not, you can stop it with the ```docker stop``` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! docker stop process-plant-simulator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 9. Next Steps \n",
    "\n",
    "Now that we have created our application, the next notebook will go over app deployment.\n",
    "\n",
    "[3-Orchestration-and-Deployment.ipynb](3-Orchestration-and-Deployment.ipynb) - After applications have been tested and validated in the sandbox or local development environments, they can be pushed to the Kelvin Platform App Registry for workload deployment on to real <a href=\"https://documentation.kelvininc.com/platform/control_center/acps/\">ACP</a> hardware as <a href=\"https://documentation.kelvininc.com/platform/control_center/workloads/\">Workloads</a>."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:root] *",
   "language": "python",
   "name": "conda-root-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
