/*
 * Copyright 2019 Kelvin Inc.
 *
 * Licensed under the Kelvin Inc. Developer SDK License Agreement (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.kelvininc.com/developer-sdk-license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */

#ifndef KELVIN_FLIGHT_MODBUS_H

#define KELVIN_FLIGHT_MODBUS_H

// Kelvin includes
#include "modbus/modbus.h"
#include "common/application/DataModel.hpp"

namespace Modbus {

namespace Default {}  // namespace Default

/// Data structure containing the dynagraph info
typedef struct {
  std::string uri;
  std::string type;
  uint32_t buffer_address;
  uint16_t buffer_size;
  uint32_t coil_address;
  float scale_multiplier;
} DynagraphInfo;

/// Data structure containing the register info
typedef struct {
  std::string uri;
  std::string input_type;
  std::string output_type;
  bool little_endian;
  bool stroke_number;
  float scale_multiplier;
  uint32_t address;

} RegisterInfo;

/// Data structure containing the connection info
typedef struct {
  uint16_t reconnect_delay;
  uint16_t retry_delay;
  uint16_t request_delay;
  uint16_t retry_attempts;
  std::string type;
  // TCP
  std::string ip;
  uint16_t port;
  // Serial
  std::string serial_port;
  int32_t baudrate;
  char parity;
  int16_t data_bits;
  int16_t stop_bits;
} ConnectionInfo;

class Modbus : public DataModelApplication {
 public:
  Modbus() : modbus_connection(nullptr) {
  }

 protected:
  /// Data structure containing the dynagraph stroke info
  typedef struct {
    uint32_t downhole_stroke = 0;
    uint32_t surface_stroke = 0;
    uint32_t stroke_number = 0;
    std::chrono::nanoseconds timestamp = std::chrono::nanoseconds(0);
  } StrokeInfo;

  StrokeInfo stroke_info;

  ConnectionInfo connection_info;

  /// Data structure containing the application parameters
  typedef struct {
    // A listing of the registrations registered with the application framework
    std::list<int32_t> registrations;
  } ApplicationProperties;

  /// The application properties
  ApplicationProperties properties;

  /// Address & RegisterInfo mapping
  std::map<uint32_t, RegisterInfo> registers_mapping;

  /// Consecutive addresses polling groups
  std::map<double, std::vector<std::vector<uint32_t>>> polling_groups;

  std::map<double, std::vector<uint32_t>> raw_polling_groups;
  std::map<double, std::vector<DynagraphInfo>> raw_dynagraph_polling_groups;

  /// Polling groups timestamps
  std::map<double, std::chrono::nanoseconds> polling_timestamps;

  /// Polling groups timestamp sync
  bool timestamp_sync;

  /// RTU configurations
  bool rtu_timestamp;
  bool connection_initialized;
  std::uint16_t minus_offset;
  std::uint16_t reconnect_delay;
  std::uint16_t request_delay;
  std::uint16_t retry_delay;
  std::uint16_t retry_attempts;
  std::uint16_t chunk_size;
  ConfigurationInterface connection;
  std::int32_t status_fault_address;
  std::int32_t status_mode_address;
  std::int32_t config_card_source_address;
  std::int32_t status_card_source_address;
  std::uint16_t card_source_value;
  bool status_card_confirmation;
  bool dynacard_validation_enabled = false;
  std::vector<std::uint16_t> status_mode_aml;

  bool debug = false;
  modbus_t* modbus_connection;

  /** @brief The configuration method that is used to setup the application.  This callback is also registered with
   *  application framework so that when changes are made to the loaded configuration file, this callback will
   *  triggered and the new configuration will be loaded.
   *
   *  @note When the configuration is reloading, any running tasks will temporarily be blocked and will
   *  not guarantee real-time performance. Data structures will be cleared and reloaded, so memory usage
   *  and task CPU usage will vary based on the incoming configuration.
   *
   *  @param[in] config The configuration to be loaded.
   */
  bool on_configuration_change(const ConfigurationInterface& config) override;

  /** Cleans up the application and releases the internally allocated resources.
   */
  void on_terminate() override;

  bool read(const double& polling_period, const std::string& register_type, const int32_t first_address,
            const std::vector<uint32_t>& polling_group);

  bool read_dynacard(const std::string& register_type, const int32_t first_address,
                     const DynagraphInfo& dynagraph_info);

  /** @brief Create a prebuilt Data Request Message request for each pair in the map.
   * @param[in] polling_period The polling period.
   * @param[in] polling_group The polling groups in the polling period.
   */
  void poll_data(const double& polling_period, const std::vector<std::vector<uint32_t>>& polling_group);

  /** @brief Establishes the Tcp/Serial Modbus connection with the RTU
   *
   * @return true if the connection was established successfully. false otherwise
   */
  bool connect();

  void disconnect();

  /** @brief Reconnects the Modbus descriptor
   * @return true if the connection was established successfully. false otherwise
   */
  bool reconnect();

  bool dynacard_validation();

  /** @brief Gets the IP for a given Hostname
   * @return IP
   */
  static std::string get_ip_by_hostname(std::string hostname);

  /** @brief Read dynagraph data.
   */
  void read_dynagraph_data(const DynagraphInfo& dynagraph_info);

  /** @brief Create a Write Request Message.
   */
  void write_data(const uint32_t address, const float float_value);

  /** @brief The callback that is triggered when new data is available.
   *  @param[in] msgs The messages that have been buffered since the last trigger.
   */
  void on_data(std::list<std::unique_ptr<MessageInterface>>& msgs) override;

  /** @brief Create a map of register addresses by polling rate
   */
  void build_raw_polling_groups();

  void group_polling_addresses();

  uint16_t calculate_response_size(const std::vector<uint32_t>& register_addresses);
};

}  // namespace Modbus

// Include the template specializations and inline methods
#include "kcf_app_modbus_cpp.icc"

#endif  // KELVIN_FLIGHT_MODBUS_H
