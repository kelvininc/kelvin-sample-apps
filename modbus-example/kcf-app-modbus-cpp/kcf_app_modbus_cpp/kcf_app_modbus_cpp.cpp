
// Kelvin includes
#include "kcf_app_modbus_cpp.h"
#include "common/descriptor/Utils.hpp"
#include "message/raw/Float32.h"
#include "message/raw/Int32.h"
#include "message/raw/Int64.h"
#include "message/raw/Dynacard.h"
#include "common/configuration/JSON.hpp"
#include "string"
#include "iostream"
#include "cmath"
#include "netdb.h"
#include "arpa/inet.h"

namespace Modbus {

namespace Default {
static constexpr uint32_t ANALOG_OUTPUT_HOLDING_REGISTERS_FUNCTION_CODE = 4;
static constexpr uint32_t ANALOG_INPUT_REGISTERS_FUNCTION_CODE = 3;
static constexpr uint32_t DISCRETE_OUTPUT_COILS_FUNCTION_CODE = 0;
static constexpr uint32_t DISCRETE_INPUT_CONTACTS_FUNCTION_CODE = 1;
static constexpr uint32_t ADDRESS_MAX = 465535;
static constexpr uint32_t DOWNHOLE_BUFFER_SIZE = 209;
static constexpr uint32_t SURFACE_BUFFER_SIZE = 407;
static const std::string DOWNHOLE_TYPE = "downhole";
static const std::string SURFACE_TYPE = "surface";
static const std::string DISCRETE_OUTPUT_COILS = "DISCRETE_OUTPUT_COILS";      // 1-9999
static const std::string DISCRETE_INPUT_CONTACTS = "DISCRETE_INPUT_CONTACTS";  //
static const std::string ANALOG_INPUT_REGISTERS = "ANALOG_INPUT_REGISTERS";
static const std::string ANALOG_OUTPUT_HOLDING_REGISTERS = "ANALOG_OUTPUT_HOLDING_REGISTERS";

}  // namespace Default

std::string Modbus::get_ip_by_hostname(std::string hostname) {
  hostent* record = gethostbyname(hostname.c_str());

  if(record) {
    in_addr address = *reinterpret_cast<in_addr*>(record->h_addr);
    std::string ip_address = inet_ntoa(address);

    return ip_address;
  } else {
    LOG_ERROR("# Modbus # Unable to get IP from Hostname: %s\n", hostname.c_str());

    return hostname;
  }
}

bool Modbus::dynacard_validation() {
  if(status_fault_address) {
    std::vector<uint16_t> response_values(1);

    const auto response_elements =
        modbus_read_input_registers(modbus_connection, status_fault_address - minus_offset, 1, &response_values[0]);

    if(response_elements != -1) {
      LOG_DEBUG("# Modbus # Status Fault Value: %d\n", response_values[0]);

      const auto status_load_fault = response_values[0] & 1u;
      const auto status_position_fault = response_values[0] & 2u;

      if(status_load_fault == 1) {
        LOG_WARN("%s", "# Modbus Dynacard # Status Load Fault\n");

        return false;
      }

      if(status_position_fault == 1) {
        LOG_WARN("%s", "# Modbus Dynacard # Status Position Fault\n");

        return false;
      }
    } else {
      LOG_ERROR("%s", "# Modbus # Unable to read Status Faults\n");

      return false;
    }
  }

  if(status_mode_address) {
    std::vector<uint16_t> response_values(1);

    const auto response_elements =
        modbus_read_input_registers(modbus_connection, status_mode_address - minus_offset, 1, &response_values[0]);

    if(response_elements != -1) {
      LOG_DEBUG("# Modbus # Status Mode Value: %d\n", response_values[0]);

      bool exists = std::find(std::begin(status_mode_aml), std::end(status_mode_aml), response_values[0]) !=
                    std::end(status_mode_aml);

      if(!exists) {
        LOG_WARN("%s", "# Modbus Dynacard # Status Mode Error\n");

        return false;
      }
    } else {
      LOG_ERROR("%s", "# Modbus # Unable to read Status Mode\n");

      return false;
    }
  }

  if(status_card_source_address && config_card_source_address) {
    // Read Config Card Source
    std::vector<uint16_t> response_values(1);

    auto response_elements =
        modbus_read_registers(modbus_connection, config_card_source_address - minus_offset, 1, &response_values[0]);

    if(response_elements != -1) {
      LOG_DEBUG("# Modbus # Config Card Source Value: %d\n", response_values[0]);

      if(response_values[0] != card_source_value) {
        response_elements =
            modbus_write_register(modbus_connection, config_card_source_address - minus_offset, card_source_value);

        if(response_elements != -1) {
          LOG_DEBUG("# Modbus # Wrote Config Card Source Value: %d\n", card_source_value);
        } else {
          LOG_ERROR("%s", "# Modbus # Unable to write Config Card Source\n");

          return false;
        }
      }
    } else {
      LOG_ERROR("%s", "# Modbus # Unable to read Config Card Source\n");

      return false;
    }

    // Read Status Card Source
    response_elements = modbus_read_input_registers(modbus_connection, status_card_source_address - minus_offset, 1,
                                                    &response_values[0]);

    if(response_elements != -1) {
      LOG_DEBUG("# Modbus # Status Card Source Value: %d\n", response_values[0]);

      if(response_values[0] != card_source_value) {
        LOG_WARN("# Modbus Dynacard # Status Card Source is %d instead of %d. Retrying...\n", response_values[0],
                 card_source_value);

        auto retries = 40;

        while((response_values[0] != card_source_value) && retries <= 40) {
          response_elements = modbus_read_input_registers(modbus_connection, status_card_source_address - minus_offset,
                                                          1, &response_values[0]);

          if(response_elements != -1) {
            LOG_DEBUG("# Modbus # Status Card Source Value: %d\n", response_values[0]);

            if(response_values[0] == card_source_value) {
              break;
            }
          } else {
            LOG_ERROR("%s", "# Modbus # Unable to read Status Mode\n");

            if(retries == 40) {
              return false;
            }
          }

          utils::time::real::sleep(std::chrono::milliseconds(100));

          retries++;
        }
      }
    } else {
      LOG_ERROR("%s", "# Modbus # Unable to read Status Card Source\n");

      return false;
    }
  }

  return true;
}

bool Modbus::read_dynacard(const std::string& register_type, const int32_t first_address,
                           const DynagraphInfo& dynagraph_info) {
  auto last_chunk_size = static_cast<u_int16_t>(dynagraph_info.buffer_size % chunk_size);
  auto total_chunks = static_cast<u_int16_t>(
      std::ceil(static_cast<float>(dynagraph_info.buffer_size) / static_cast<float>(chunk_size)));
  auto chunk_offset = 0;
  auto aux_chunk_size = chunk_size;

  std::vector<uint16_t> response_values(dynagraph_info.buffer_size);

  for(auto i = 1; i <= total_chunks; i++) {
    if(i == total_chunks) {
      aux_chunk_size = last_chunk_size;
    }

    auto success = false;
    auto retries = 0;

    while(!success && retries <= retry_attempts) {
      try {
        if(utils::is_equal(register_type, Default::ANALOG_INPUT_REGISTERS)) {
          auto response_code =
              modbus_read_input_registers(modbus_connection, first_address + chunk_offset - minus_offset,
                                          aux_chunk_size, &response_values[static_cast<u_int16_t>(chunk_offset)]);

          if(response_code == -1) {
            LOG_ERROR("%s", "# Modbus # Read Dynacard # Error: Unable to read chunk\n");

            reconnect();
            retries++;
          } else {
            chunk_offset = chunk_offset + aux_chunk_size;
            success = true;
          }
        } else if(utils::is_equal(register_type, Default::ANALOG_OUTPUT_HOLDING_REGISTERS)) {
          auto response_code =
              modbus_read_registers(modbus_connection, first_address + chunk_offset - minus_offset, aux_chunk_size,
                                    &response_values[static_cast<u_int16_t>(chunk_offset)]);
          if(response_code == -1) {
            LOG_ERROR("%s", "# Modbus # Read Dynacard # ERROR: Unable to read chunk\n");

            reconnect();
            retries++;
          } else {
            chunk_offset = chunk_offset + aux_chunk_size;
            success = true;
          }
        } else {
          LOG_ERROR("# Modbus # Read Dynacard # ERROR: Register type not found: %s\n", register_type.c_str());

          return false;
        }
      } catch(const std::exception& e) {
        LOG_ERROR("# Modbus # Read Dynacard # ERROR: %s\n", e.what());

        reconnect();
        retries++;
      }

      if(!success) {
        if(retries <= retry_attempts) {
          LOG_WARN("# Modbus # Read Dynacard # Attempt: %d - Retrying in %d second(s)...\n", retries, retry_delay);
          utils::time::real::sleep(std::chrono::seconds(retry_delay));
        } else {
          LOG_ERROR("# Modbus # Read Dynacard # ERROR: Unable to read Dynacard: %s\n", dynagraph_info.uri.c_str());

          return false;
        }
      }
    }
  }

  if(dynacard_validation_enabled) {
    if(status_card_confirmation) {
      // Read Status Card Source
      const auto response_elements = modbus_read_input_registers(
          modbus_connection, status_card_source_address - minus_offset, 1, &response_values[0]);

      if(response_elements != -1) {
        LOG_DEBUG("# Modbus # Status Card Source Value: %d\n", response_values[0]);

        if(response_values[0] != card_source_value) {
          LOG_ERROR("%s", "# Modbus Dynacard # Status Card Source changed while reading Dynacard. Discarding data.\n");

          return false;
        }
      } else {
        LOG_ERROR("%s", "# Modbus Dynacard # Unable to read Status Card Source. Discarding data.\n");

        return false;
      }
    }
  }

  std::stringstream pos;
  std::stringstream load;
  std::vector<double> pos_values;
  std::vector<double> load_values;

  auto dynacard_timestamp = stroke_info.timestamp;

  if(rtu_timestamp) {
    uint16_t rtu_timestamp_values[2] = {response_values[1], response_values[0]};

    auto rtu_timestamp_seconds = *reinterpret_cast<const uint32_t*>(rtu_timestamp_values);

    LOG_DEBUG("# Modbus Dynacard # RTU Timestamp: %d\n", rtu_timestamp_seconds);

    if(rtu_timestamp_seconds != 0) {
      dynacard_timestamp = std::chrono::seconds(rtu_timestamp_seconds);
    }
  }

  for(uint16_t i = dynagraph_info.buffer_size % 100; i < dynagraph_info.buffer_size - 1; i += 2) {
    pos_values.emplace_back(static_cast<double>(response_values[i]) / static_cast<double>(100) *
                            static_cast<double>(dynagraph_info.scale_multiplier));
    load_values.emplace_back(static_cast<double>(response_values[i + 1]) *
                             static_cast<double>(dynagraph_info.scale_multiplier));

    if(debug) {
      pos << " "
          << static_cast<double>(response_values[i]) / static_cast<double>(100) *
                 static_cast<double>(dynagraph_info.scale_multiplier);
      load << " " << static_cast<double>(response_values[i + 1]) * static_cast<double>(dynagraph_info.scale_multiplier);
    }
  }

  if(debug) {
    LOG_DEBUG("# Modbus Dynacard # Position Values: [%s]\n", pos.str().c_str());
    LOG_DEBUG("# Modbus Dynacard # Load Values: [%s]\n\n", load.str().c_str());

    std::stringstream decimal_values;
    std::stringstream hex_values;

    for(auto response_value : response_values) {
      decimal_values << " " << response_value;
      hex_values << " " << std::hex << response_value;
    }

    LOG_DEBUG("# Modbus Dynacard # Decimal Values: [%s]\n", decimal_values.str().c_str());
    LOG_DEBUG("# Modbus Dynacard # Hex Values: [%s]\n", hex_values.str().c_str());
  }

  // Build and publish the values into the Bus
  auto msg = std::make_unique<message::raw::Dynacard>();

  msg->set_stroke_number(static_cast<int64_t>(stroke_info.stroke_number));
  msg->set_load(load_values);
  msg->set_position(pos_values);
  msg->header->set_name(dynagraph_info.uri);

  if(timestamp_sync) {
    msg->header->set_time_of_validity(stroke_info.timestamp);
  } else {
    msg->header->set_time_of_validity(dynacard_timestamp);
  }

  LOG_INFO_DATAMODEL(msg);

  publish(std::move(msg));

  return true;
}

bool Modbus::read(const double& polling_period, const std::string& register_type, const int32_t first_address,
                  const std::vector<uint32_t>& polling_group) {
  auto success = false;
  auto retries = 0;

  const auto polling_timestamp = polling_timestamps.find(polling_period)->second;

  while(!success && retries <= retry_attempts) {
    try {
      if(utils::is_equal(register_type, Default::DISCRETE_OUTPUT_COILS)) {
        const auto response_size = static_cast<u_int16_t>(polling_group.size() * 8);
        std::vector<uint8_t> response_values(response_size);

        const auto response_elements =
            modbus_read_bits(modbus_connection, first_address, response_size, &response_values[0]);

        if(debug) {
          std::stringstream response_hex_values;

          for(auto response_value : response_values) {
            response_hex_values << " " << std::hex << response_value;
          }

          LOG_DEBUG("# Modbus # Response Values (HEX): [%s]\n", response_hex_values.str().c_str());
        }

        if(response_elements != -1) {
          uint16_t address_index = 0;

          if(response_size == static_cast<uint16_t>(response_elements)) {
            for(uint16_t i = 0; i < static_cast<uint16_t>(response_values.capacity()); i += 8) {
              // Float
              const auto value = static_cast<float>(modbus_get_byte_from_bits(&response_values[0], i, 8));

              auto msg = std::make_unique<message::raw::Float32>();

              const auto register_info = registers_mapping.find(polling_group[address_index])->second;
              msg->header->set_name(register_info.uri);
              msg->set_value(value * register_info.scale_multiplier);
              if(timestamp_sync) {
                msg->header->set_time_of_validity(polling_timestamp);
              }

              LOG_INFO_DATAMODEL(msg);

              publish(std::move(msg));

              address_index++;
            }

            success = true;
          } else {
            LOG_ERROR("%s", "# Modbus ERROR # Request/Response Length Mismatch\n");
            LOG_DEBUG("# Modbus # Response_elements: %d\n", response_elements);
            LOG_DEBUG("# Modbus # Response Size: %d\n", response_size);

            reconnect();
            retries++;
          }
        } else {
          LOG_ERROR("%s", "# Modbus ERROR # Unable to Read\n");

          reconnect();
          retries++;
        }
      } else if(utils::is_equal(register_type, Default::DISCRETE_INPUT_CONTACTS)) {
        const auto response_size = static_cast<u_int16_t>(polling_group.size() * 8);
        std::vector<uint8_t> response_values(response_size);

        const auto response_elements =
            modbus_read_input_bits(modbus_connection, first_address, response_size, &response_values[0]);

        if(debug) {
          std::stringstream response_hex_values;

          for(auto response_value : response_values) {
            response_hex_values << " " << std::hex << response_value;
          }

          LOG_DEBUG("# Modbus # Response Values (HEX): [%s]\n", response_hex_values.str().c_str());
        }

        if(response_elements != -1) {
          uint16_t address_index = 0;

          if(response_size == static_cast<uint16_t>(response_elements)) {
            for(uint16_t i = 0; i < static_cast<uint16_t>(response_values.capacity()); i += 8) {
              // Float
              const auto value = static_cast<float>(modbus_get_byte_from_bits(&response_values[0], i, 8));

              auto msg = std::make_unique<message::raw::Float32>();

              const auto register_info = registers_mapping.find(polling_group[address_index])->second;
              msg->header->set_name(register_info.uri);
              msg->set_value(value * register_info.scale_multiplier);
              if(timestamp_sync) {
                msg->header->set_time_of_validity(polling_timestamp);
              }

              LOG_INFO_DATAMODEL(msg);

              publish(std::move(msg));

              address_index++;
            }

            success = true;
          } else {
            LOG_ERROR("%s", "# Modbus ERROR # Values Mismatch\n");
            LOG_DEBUG("# Modbus # Response_elements: %d\n", response_elements);
            LOG_DEBUG("# Modbus # Response Size: %d\n", response_size);

            reconnect();
            retries++;
          }
        } else {
          LOG_ERROR("%s", "# Modbus ERROR # Unable to Read\n");

          reconnect();
          retries++;
        }
      } else if(utils::is_equal(register_type, Default::ANALOG_INPUT_REGISTERS)) {
        const auto response_size = calculate_response_size(polling_group);
        std::vector<uint16_t> response_values(response_size);

        const auto response_elements =
            modbus_read_input_registers(modbus_connection, first_address, response_size, &response_values[0]);

        if(debug) {
          std::stringstream response_hex_values;

          for(auto response_value : response_values) {
            response_hex_values << " " << std::hex << response_value;
          }

          LOG_DEBUG("# Modbus # Response Values (HEX): [%s]\n", response_hex_values.str().c_str());
        }

        if(response_elements != -1) {
          if(response_size == static_cast<uint16_t>(response_elements)) {
            uint16_t address_index = 0;

            for(uint16_t i = 0; i < static_cast<uint16_t>(response_values.capacity()); i++) {
              const auto register_info = registers_mapping.find(polling_group[address_index])->second;

              if(utils::is_equal(register_info.input_type, "int32")) {
                int32_t value;

                if(register_info.little_endian) {
                  value = response_values[i] + (response_values[i + 1] << 16);
                } else {
                  value = (response_values[i] << 16) + response_values[i + 1];
                }

                i++;

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int32_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int64_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else if(utils::is_equal(register_info.input_type, "uint32")) {
                int32_t value;

                if(register_info.little_endian) {
                  value = response_values[i] + (response_values[i + 1] << 16);
                } else {
                  value = (response_values[i] << 16) + response_values[i + 1];
                }

                i++;

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int32_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int64_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else if(utils::is_equal(register_info.input_type, "int16") ||
                        utils::is_equal(register_info.input_type, "uint16")) {
                const auto value = response_values[i];

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(value * static_cast<int32_t>(register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(value * static_cast<int64_t>(register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else if(utils::is_equal(register_info.input_type, "float32")) {
                uint16_t element_value[2] = {response_values[i + 1], response_values[i]};
                const auto value = *reinterpret_cast<const float*>(element_value);

                i++;

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int32_t>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int64_t>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else {
                LOG_ERROR("# Modbus ERROR # Unknown register type: %s\n", register_info.input_type.c_str());
              }

              address_index++;
            }

            success = true;
          } else {
            LOG_ERROR("%s", "# Modbus ERROR # Values Mismatch\n");
            LOG_DEBUG("# Modbus # Response_elements: %d\n", response_elements);
            LOG_DEBUG("# Modbus # Response Size: %d\n", response_size);

            reconnect();
            retries++;
          }
        } else {
          LOG_ERROR("%s", "# Modbus ERROR # Unable to Read\n");

          reconnect();
          retries++;
        }
      } else if(utils::is_equal(register_type, Default::ANALOG_OUTPUT_HOLDING_REGISTERS)) {
        const auto response_size = calculate_response_size(polling_group);
        std::vector<uint16_t> response_values(response_size);

        const auto response_elements =
            modbus_read_registers(modbus_connection, first_address, response_size, &response_values[0]);

        if(debug) {
          std::stringstream response_hex_values;

          for(auto response_value : response_values) {
            response_hex_values << " " << std::hex << response_value;
          }

          LOG_DEBUG("# Modbus # Response Values (HEX): [%s]\n", response_hex_values.str().c_str());
        }

        if(response_elements != -1) {
          if(response_size == static_cast<uint16_t>(response_elements)) {
            uint16_t address_index = 0;

            for(uint16_t i = 0; i < static_cast<uint16_t>(response_values.capacity()); i++) {
              uint16_t element_value[2] = {response_values[i + 1], response_values[i]};

              const auto register_info = registers_mapping.find(polling_group[address_index])->second;

              if(utils::is_equal(register_info.input_type, "int32")) {
                int32_t value;

                if(register_info.little_endian) {
                  value = response_values[i] + (response_values[i + 1] << 16);
                } else {
                  value = (response_values[i] << 16) + response_values[i + 1];
                }

                i++;

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int32_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int64_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else if(utils::is_equal(register_info.input_type, "uint32")) {
                int32_t value;

                if(register_info.little_endian) {
                  value = response_values[i] + (response_values[i + 1] << 16);
                } else {
                  value = (response_values[i] << 16) + response_values[i + 1];
                }

                i++;

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int32_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int64_t>(static_cast<float>(value) * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else if(utils::is_equal(register_info.input_type, "int16") ||
                        utils::is_equal(register_info.input_type, "uint16")) {
                const auto value = response_values[i];

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(value * static_cast<int32_t>(register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(value * static_cast<int64_t>(register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value) * register_info.scale_multiplier);

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else if(utils::is_equal(register_info.input_type, "float32")) {
                const auto value = *reinterpret_cast<const float*>(element_value);

                i++;

                std::chrono::nanoseconds msg_time_of_validity;

                if(utils::is_equal(register_info.output_type, "raw.float32")) {
                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int32")) {
                  auto msg = std::make_unique<message::raw::Int32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int32_t>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else if(utils::is_equal(register_info.output_type, "raw.int64")) {
                  auto msg = std::make_unique<message::raw::Int64>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<int64_t>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                } else {
                  LOG_WARN("# Modbus # Unknown output_type: %s. Using default: raw.float32\n",
                           register_info.output_type.c_str());

                  auto msg = std::make_unique<message::raw::Float32>();

                  msg->header->set_name(register_info.uri);
                  msg->set_value(static_cast<float>(value * register_info.scale_multiplier));

                  if(timestamp_sync) {
                    msg->header->set_time_of_validity(polling_timestamp);
                  }

                  msg_time_of_validity = msg->header->get_time_of_validity();

                  LOG_INFO_DATAMODEL(msg);

                  publish(std::move(msg));
                }

                if(register_info.stroke_number && static_cast<uint32_t>(value) != 0) {
                  stroke_info.stroke_number = static_cast<uint32_t>(value);

                  if(timestamp_sync) {
                    stroke_info.timestamp = polling_timestamp;
                  } else {
                    stroke_info.timestamp = msg_time_of_validity;
                  }
                }
              } else {
                LOG_ERROR("# Modbus ERROR # Unknown register type: %s\n", register_info.input_type.c_str());
              }

              address_index++;
            }

            success = true;
          } else {
            LOG_ERROR("%s", "# Modbus ERROR # Values Mismatch\n");
            LOG_DEBUG("# Modbus # Response_elements: %d\n", response_elements);
            LOG_DEBUG("# Modbus # Response Size: %d\n", response_size);

            reconnect();
            retries++;
          }
        }  // namespace Modbus
        else {
          LOG_ERROR("%s", "# Modbus ERROR # Unable to Read\n");

          reconnect();
          retries++;
        }
      } else {
        LOG_ERROR("# Modbus # Read ERROR: Register type not found: %s\n", register_type.c_str());

        return false;
      }
    } catch(const std::exception& e) {
      LOG_ERROR("# Modbus # Read ERROR: %s\n", e.what());

      retries++;
    }

    if(!success) {
      LOG_ERROR("# Modbus # Attempt: %d - Retrying in %d seconds...\n", retries, retry_delay);
      utils::time::real::sleep(std::chrono::seconds(retry_delay));
    }
  }

  return success;
}

void Modbus::disconnect() {
  auto response_code = modbus_flush(modbus_connection);
  LOG_DEBUG("# Modbus # Flushed %d bytes from the buffer\n", response_code);

  modbus_close(modbus_connection);
  modbus_free(modbus_connection);
}

bool Modbus::reconnect() {
  disconnect();

  return connect();
}

bool Modbus::connect() {
  auto connected = false;

  while(!connected) {
    try {
      std::string connection_type;

      if(connection["type"].IsDefined()) {
        connection_type = connection["type"].as<std::string>();
      } else {
        LOG_ERROR("%s", "# Modbus ERROR # Missing Configuration key: connection:type\n");

        return false;
      }

      reconnect_delay = connection["reconnect_delay"].as<std::uint16_t>(15);
      retry_delay = connection["retry_delay"].as<std::uint16_t>(0);
      request_delay = connection["request_delay"].as<std::uint16_t>(0);
      retry_attempts = connection["retry_attempts"].as<std::uint16_t>(5);

      if(utils::is_equal(utils::to_lower(connection_type), "tcp")) {
        if(!connection["ip"].IsDefined()) {
          LOG_ERROR("%s", "# Modbus ERROR # Missing Configuration key: connection:ip\n");

          return false;
        }

        if(!connection["port"].IsDefined()) {
          LOG_ERROR("%s", "# Modbus ERROR # Missing Configuration key: connection:port\n");

          return false;
        }

        const auto host_ip = get_ip_by_hostname(connection["ip"].as<std::string>());

        modbus_connection = modbus_new_tcp(host_ip.c_str(), connection["port"].as<uint16_t>());
      } else if(utils::is_equal(utils::to_lower(connection_type), "serial")) {
        if(!connection["serial_port"].IsDefined()) {
          LOG_ERROR("%s", "# Modbus ERROR # Missing Configuration key: connection:serial_port\n");

          return false;
        }

        if(!connection["baudrate"].IsDefined()) {
          LOG_ERROR("%s", "# Modbus ERROR # Missing Configuration key: connection:baudrate\n");

          return false;
        }

        modbus_connection =
            modbus_new_rtu(connection["serial_port"].as<std::string>().c_str(), connection["baudrate"].as<int32_t>(),
                           connection["parity"].as<char>('N'), connection["data_bits"].as<std::int16_t>(8),
                           connection["stop_bits"].as<std::int16_t>(1));
      } else {
        LOG_ERROR("# Modbus ERROR # Unknown connection type: %s\n", connection_type.c_str());

        return false;
      }

      if(modbus_connect(modbus_connection) == -1) {
        LOG_ERROR("# Modbus ERROR # %s\n", modbus_strerror(errno));
        LOG_ERROR("# Modbus # Retrying in %d seconds...\n", reconnect_delay);

        utils::time::real::sleep(std::chrono::seconds(reconnect_delay));

        continue;
      } else {
        LOG_DEBUG("%s", "# Modbus # Connected\n");

        if(utils::is_equal(utils::to_lower(connection_type), "serial")) {
          if(connection["error_recovery"].as<bool>(false)) {
            modbus_set_error_recovery(modbus_connection, MODBUS_ERROR_RECOVERY_LINK);
          }

          if(connection["serial_mode"].IsDefined()) {
            const auto serial_mode = connection["serial_mode"].as<std::string>();

            if(utils::is_equal(utils::to_upper(serial_mode), "RS232")) {
              if(modbus_rtu_set_serial_mode(modbus_connection, MODBUS_RTU_RS232) == -1) {
                LOG_ERROR("# Modbus # Failed to set serial mode: %s\n", modbus_strerror(errno));
              } else {
                LOG_INFO("# Modbus # Serial mode set to: %s\n", serial_mode.c_str());
              }
            } else if(utils::is_equal(utils::to_upper(serial_mode), "RS485")) {
              if(modbus_rtu_set_serial_mode(modbus_connection, MODBUS_RTU_RS485) == -1) {
                LOG_ERROR("# Modbus # Failed to set serial mode: %s\n", modbus_strerror(errno));
              } else {
                LOG_INFO("# Modbus # Serial mode set to: %s\n", serial_mode.c_str());
              }
            } else {
              LOG_ERROR("# Modbus ERROR # Unsupported Serial Mode: %s\n", serial_mode.c_str());

              return false;
            }
          }

          if(connection["rts_mode"].IsDefined()) {
            const auto rts_mode = connection["rts_mode"].as<std::string>();

            if(utils::is_equal(utils::to_upper(rts_mode), "UP")) {
              if(modbus_rtu_set_rts(modbus_connection, MODBUS_RTU_RTS_UP) == -1) {
                LOG_ERROR("# Modbus # Failed to set RTS: %s\n", modbus_strerror(errno));
              }
            } else if(utils::is_equal(utils::to_upper(rts_mode), "DOWN")) {
              if(modbus_rtu_set_rts(modbus_connection, MODBUS_RTU_RTS_UP) == -1) {
                LOG_ERROR("# Modbus # Failed to set RTS: %s\n", modbus_strerror(errno));
              }
            } else {
              if(modbus_rtu_set_rts(modbus_connection, MODBUS_RTU_RTS_NONE) == -1) {
                LOG_ERROR("# Modbus # Failed to set RTS: %s\n", modbus_strerror(errno));
              }
            }

            if(connection["rts_delay"].IsDefined()) {
              if(modbus_rtu_set_rts_delay(modbus_connection, connection["rts_delay"].as<std::uint16_t>()) == -1) {
                LOG_ERROR("# Modbus # Failed to set rts delay: %s\n", modbus_strerror(errno));
              }
            }

            LOG_INFO("# Modbus # RTS: %d\n", modbus_rtu_get_rts(modbus_connection));
            LOG_INFO("# Modbus # RTS delay: %d\n", modbus_rtu_get_rts_delay(modbus_connection));
          } else {
            if(modbus_rtu_set_rts(modbus_connection, MODBUS_RTU_RTS_NONE) == -1) {
              LOG_ERROR("# Modbus # Failed to set RTS: %s\n", modbus_strerror(errno));
            }
          }
        }

        auto debug_enabled = modbus_set_debug(modbus_connection, debug);

        if(debug_enabled != 0) {
          LOG_ERROR("%s", "# Modbus # Unable to set connection debug mode\n");
        }

        const auto timeout = connection["timeout"].as<uint32_t>(5);

        auto timeout_enabled = modbus_set_response_timeout(modbus_connection, timeout, 0);

        if(timeout_enabled != 0) {
          LOG_ERROR("# Modbus # Unable to set connection timeout: %d\n", timeout);
        }

        if(connection["slave_id"].IsDefined()) {
          const auto slave_id = connection["slave_id"].as<std::int16_t>();

          modbus_set_slave(modbus_connection, slave_id);
        } else if(utils::is_equal(utils::to_lower(connection_type), "serial")) {
          LOG_ERROR("%s", "# Modbus ERROR # Missing Configuration key: connection:slave_id\n");

          return false;
        }

        connected = true;
      }
    } catch(const std::exception& e) {
      LOG_ERROR("# Modbus # Connection ERROR: %s\n", e.what());

      LOG_ERROR("# Modbus # Retrying in %d seconds...\n", reconnect_delay);
      utils::time::real::sleep(std::chrono::seconds(reconnect_delay));
    }
  }

  return connected;
}

void Modbus::on_data(std::list<std::unique_ptr<MessageInterface>>& msgs) {
  for(auto& msg : msgs) {
    LOG_INFO("Got Input Message: %s\n", msg->header->get_name().c_str());
    LOG_INFO_DATAMODEL(msg);

    const auto address = get_registry_map().find(msg->header->get_name())->second["address"].as<uint32_t>();
    const auto any_value = (*ConfigurationInterface::load(msg->to_json()))["value"].as<std::string>();

    if(!any_value.empty()) {
      const auto value = std::experimental::any_cast<float>(any_value);

      write_data(address, value);
    } else {
      LOG_INFO("%s\n", "Empty Value");
    }
  }
}

void Modbus::read_dynagraph_data(const DynagraphInfo& dynagraph_info) {
  if(!connection_initialized) {
    // Connect to the RTU
    connect();

    connection_initialized = true;
  }

  if(dynagraph_info.coil_address != 0) {
    // Write to Coil Address in order to update the buffer
    const auto response_code =
        modbus_write_bit(modbus_connection, static_cast<int32_t>(dynagraph_info.coil_address - minus_offset), 1);

    if(response_code != -1) {
      LOG_DEBUG("\n# Modbus # Wrote to Coil Address %d\n", dynagraph_info.coil_address);
    } else {
      LOG_ERROR("%s", "# Modbus ERROR # Unable to Write to Coil Address\n");

      reconnect();

      return;
    }
  }

  // Read the buffer
  const auto buffer_address_function_code =
      static_cast<uint32_t>(std::to_string(dynagraph_info.buffer_address).c_str()[0] - '0');
  auto buffer_address_length = std::to_string(dynagraph_info.buffer_address).length();
  uint32_t buffer_address_offset = 0;

  if(buffer_address_length > 4) {
    buffer_address_offset =
        buffer_address_function_code * static_cast<uint32_t>(pow(10, static_cast<double>(buffer_address_length) - 1));
  }

  auto first_address = static_cast<int32_t>(dynagraph_info.buffer_address - buffer_address_offset - minus_offset);

  auto success = false;

  if(dynagraph_info.type == Default::DOWNHOLE_TYPE) {
    if(stroke_info.stroke_number != stroke_info.downhole_stroke) {
      if(dynacard_validation_enabled) {
        if(!dynacard_validation()) {
          LOG_ERROR("%s", "# Modbus Dynacard # Validation Failed\n");

          return;
        }
      }

      if(buffer_address_function_code == Default::ANALOG_INPUT_REGISTERS_FUNCTION_CODE) {
        success = read_dynacard(Default::ANALOG_INPUT_REGISTERS, first_address, dynagraph_info);
      } else if(buffer_address_function_code == Default::ANALOG_OUTPUT_HOLDING_REGISTERS_FUNCTION_CODE) {
        success = read_dynacard(Default::ANALOG_OUTPUT_HOLDING_REGISTERS, first_address, dynagraph_info);
      }
    } else {
      LOG_INFO("# Modbus Dynacard # No new Downhole Card Stroke: %d\n", stroke_info.stroke_number);

      return;
    }

    if(!success) {
      return;
    }

    utils::time::real::sleep(std::chrono::seconds(request_delay));

    stroke_info.downhole_stroke = stroke_info.stroke_number;
  } else if(dynagraph_info.type == Default::SURFACE_TYPE) {
    if(stroke_info.stroke_number != stroke_info.surface_stroke) {
      if(dynacard_validation_enabled) {
        if(!dynacard_validation()) {
          LOG_ERROR("%s", "# Modbus Dynacard # Validation Failed\n");

          return;
        }
      }

      if(buffer_address_function_code == Default::ANALOG_INPUT_REGISTERS_FUNCTION_CODE) {
        success = read_dynacard(Default::ANALOG_INPUT_REGISTERS, first_address, dynagraph_info);
      } else if(buffer_address_function_code == Default::ANALOG_OUTPUT_HOLDING_REGISTERS_FUNCTION_CODE) {
        success = read_dynacard(Default::ANALOG_OUTPUT_HOLDING_REGISTERS, first_address, dynagraph_info);
      }
    } else {
      LOG_INFO("# Modbus Dynacard # No new Surface Card Stroke: %d\n", stroke_info.stroke_number);

      return;
    }
    utils::time::real::sleep(std::chrono::seconds(request_delay));

    if(!success) {
      return;
    }

    stroke_info.surface_stroke = stroke_info.stroke_number;
  }
}

void Modbus::write_data(const uint32_t address, const float float_value) {
  if(!connection_initialized) {
    // Connect to the RTU
    connect();

    connection_initialized = true;
  }

  auto address_function_code = static_cast<uint32_t>(std::to_string(address).c_str()[0] - '0');
  auto address_length = std::to_string(address).length();
  uint32_t address_offset = 0;

  if(address_length > 4) {
    address_offset = address_function_code * static_cast<uint32_t>(pow(10, static_cast<double>(address_length) - 1));
  } else {
    address_function_code = 0;
  }

  /// Discrete Output Coils
  if(address_function_code == Default::DISCRETE_OUTPUT_COILS_FUNCTION_CODE) {
    const auto byte_value = static_cast<uint8_t>(float_value);

    uint8_t value[8];
    modbus_set_bits_from_byte(value, 0, byte_value);

    const auto response_elements =
        modbus_write_bits(modbus_connection, static_cast<int32_t>(address - address_offset - minus_offset), 8, value);

    if(response_elements != -1) {
      LOG_INFO("\n# Modbus # Wrote %d to address %d\n\n", byte_value, address);
    } else {
      LOG_ERROR("%s", "# Modbus ERROR # Unable to Write\n");

      reconnect();
    }
  }
  /// Discrete Input Contacts
  else if(address_function_code == Default::DISCRETE_INPUT_CONTACTS_FUNCTION_CODE) {
    LOG_ERROR("# Modbus ERROR # Write to address %d is not supported\n", address);
  }
  /// Analog Input Registers
  else if(address_function_code == Default::ANALOG_INPUT_REGISTERS_FUNCTION_CODE) {
    LOG_ERROR("# Modbus ERROR # Write to address %d is not supported\n", address);
  }
  /// Analog Output Holding Registers
  else if(address_function_code == Default::ANALOG_OUTPUT_HOLDING_REGISTERS_FUNCTION_CODE) {
    uint16_t value[2];
    modbus_set_float_dcba(float_value, value);

    const auto response_elements = modbus_write_registers(
        modbus_connection, static_cast<int32_t>(address - address_offset - minus_offset), 2, value);

    if(response_elements != -1) {
      LOG_INFO("\n# Modbus # Wrote %f to address %d\n", float_value, address);
    } else {
      LOG_ERROR("%s", "# Modbus ERROR # Unable to Write\n");

      reconnect();
    }
  } else {
    LOG_ERROR("# Modbus ERROR # Address out of range: %d\n", address);
  }

  utils::time::real::sleep(std::chrono::seconds(request_delay));
}

uint16_t Modbus::calculate_response_size(const std::vector<uint32_t>& register_addresses) {
  uint16_t amount_of_registers_to_read = 0;
  for(const auto address : register_addresses) {
    std::string register_type = registers_mapping.find(address)->second.input_type;

    if(utils::is_equal(register_type, "uint32") || utils::is_equal(register_type, "int32") ||
       utils::is_equal(register_type, "float32")) {
      amount_of_registers_to_read += 2;
      // @todo: [S1-161] check this 16 bit messages
    } else if(utils::is_equal(register_type, "uint16") || utils::is_equal(register_type, "int16")) {
      amount_of_registers_to_read++;
    }
  }

  return amount_of_registers_to_read;
}

void Modbus::poll_data(const double& polling_period, const std::vector<std::vector<uint32_t>>& polling_group) {
  if(polling_timestamps.find(polling_period) == polling_timestamps.end()) {
    polling_timestamps.emplace(polling_period, utils::time::replay::now());
  } else {
    polling_timestamps.find(polling_period)->second = utils::time::replay::now();
  }

  if(!connection_initialized) {
    // Connect to the RTU
    connect();

    connection_initialized = true;
  }

  for(const auto& sequential_group : polling_group) {
    LOG_DEBUG("# Modbus # Polling Period: %fs\n", polling_period);

    auto first_address = static_cast<int32_t>(sequential_group[0]);

    auto first_address_function_code = static_cast<uint32_t>(std::to_string(first_address).c_str()[0] - '0');
    auto first_address_length = std::to_string(first_address).length();
    uint32_t first_address_offset = 0;

    if(first_address_length > 4) {
      first_address_offset =
          first_address_function_code * static_cast<uint32_t>(pow(10, static_cast<double>(first_address_length) - 1));
    } else {
      first_address_function_code = 0;
    }

    first_address = (first_address - static_cast<int32_t>(first_address_offset) - static_cast<int32_t>(minus_offset));

    /// Discrete Output Coils
    if(first_address_function_code == Default::DISCRETE_OUTPUT_COILS_FUNCTION_CODE) {
      read(polling_period, Default::DISCRETE_OUTPUT_COILS, first_address, sequential_group);
    }
    /// Discrete Input Contacts
    else if(first_address_function_code == Default::DISCRETE_INPUT_CONTACTS_FUNCTION_CODE) {
      read(polling_period, Default::DISCRETE_INPUT_CONTACTS, first_address, sequential_group);
    }
    /// Analog Input Registers
    else if(first_address_function_code == Default::ANALOG_INPUT_REGISTERS_FUNCTION_CODE) {
      read(polling_period, Default::ANALOG_INPUT_REGISTERS, first_address, sequential_group);
    }
    /// Analog Output Holding Registers
    else if(first_address_function_code == Default::ANALOG_OUTPUT_HOLDING_REGISTERS_FUNCTION_CODE) {
      read(polling_period, Default::ANALOG_OUTPUT_HOLDING_REGISTERS, first_address, sequential_group);
    } else {
      LOG_ERROR("# Modbus ERROR # Address out of range: %d\n", first_address);
    }

    utils::time::real::sleep(std::chrono::seconds(request_delay));
  }
}

void Modbus::build_raw_polling_groups() {
  for(const auto& pair : get_registry_map()) {
    double period;

    if(ConfigurationInterface::has_key(pair.second, "period")) {
      period = pair.second["period"].as<double>();
    } else {
      std::stringstream ss;
      ss << "# Modbus ERROR # Missing Register key: period" << std::endl
         << "# Modbus ERROR # Discarding Topic: " << pair.first << std::endl;
      LOG_ERROR("%s", ss.str().c_str());

      continue;
    }
    if(ConfigurationInterface::has_key(pair.second, "type")) {
      if(utils::is_equal(pair.second["type"].as<std::string>(), "raw.dynacard")) {
        auto dynagraph_info = pair.second.as<::Modbus::DynagraphInfo>();
        dynagraph_info.uri = pair.first;

        if(dynagraph_info.coil_address >= Default::ADDRESS_MAX || dynagraph_info.buffer_address <= 0 ||
           dynagraph_info.buffer_address >= Default::ADDRESS_MAX) {
          std::stringstream ss;
          ss << "# Modbus ERROR # Register address out of range (0 < address " << Default::ADDRESS_MAX
             << "): " << static_cast<uint32_t>(dynagraph_info.coil_address) << std::endl
             << "# Modbus ERROR # Discarding Register: " << dynagraph_info.uri << std::endl;
          LOG_ERROR("%s", ss.str().c_str());

          continue;
        }

        if(dynagraph_info.buffer_size != Default::DOWNHOLE_BUFFER_SIZE &&
           dynagraph_info.buffer_size != Default::SURFACE_BUFFER_SIZE) {
          std::stringstream ss;
          ss << "# Modbus ERROR # Unknown Dynagraph buffer_size: " << static_cast<uint32_t>(dynagraph_info.buffer_size)
             << std::endl
             << "# Modbus ERROR # Discarding Register: " << dynagraph_info.uri << std::endl;
          LOG_ERROR("%s", ss.str().c_str());

          continue;
        }

        if(raw_dynagraph_polling_groups.find(period) == raw_dynagraph_polling_groups.end()) {
          raw_dynagraph_polling_groups.emplace(period, std::vector<DynagraphInfo>());
        }

        raw_dynagraph_polling_groups.find(period)->second.emplace_back(dynagraph_info);
      } else {
        RegisterInfo register_info = pair.second.as<::Modbus::RegisterInfo>();
        register_info.uri = pair.first;

        if(register_info.address <= 0 || register_info.address >= Default::ADDRESS_MAX) {
          std::stringstream ss;
          ss << "# Modbus ERROR # Register address out of range (0 < address " << Default::ADDRESS_MAX
             << "): " << static_cast<uint32_t>(register_info.address) << std::endl
             << "# Modbus ERROR # Discarding Register: " << pair.first << std::endl;
          LOG_ERROR("%s", ss.str().c_str());

          continue;
        }

        if(raw_polling_groups.find(period) == raw_polling_groups.end()) {
          raw_polling_groups.emplace(period, std::vector<uint32_t>());
        }

        raw_polling_groups.find(period)->second.emplace_back(register_info.address);

        registers_mapping.emplace(register_info.address, register_info);
      }
    } else {
      std::stringstream ss;
      ss << "# Modbus ERROR # Missing Register key: type" << std::endl
         << "# Modbus ERROR # Discarding Topic: " << pair.first << std::endl;
      LOG_ERROR("%s", ss.str().c_str());

      continue;
    }
  }
}

void Modbus::group_polling_addresses() {
  for(auto& polling_group : raw_polling_groups) {
    std::sort(polling_group.second.begin(), polling_group.second.end());

    uint32_t last_register = 0;

    std::vector<std::vector<uint32_t>> address_sequences;
    std::vector<uint32_t> sequence_group;

    for(decltype(polling_group.second.size()) i = 0; i < polling_group.second.size(); i++) {
      auto first_address_function_code =
          static_cast<uint32_t>(std::to_string(polling_group.second[i]).c_str()[0] - '0');
      auto first_address_length = std::to_string(polling_group.second[i]).length();

      if(first_address_length <= 4) {
        first_address_function_code = 0;
      }

      if((((polling_group.second[i] - last_register == 2 &&
            (utils::contains(registers_mapping.find(last_register)->second.input_type, "32") ||
             utils::contains(registers_mapping.find(last_register)->second.input_type, "float"))) ||
           polling_group.second[i] - last_register == 1) &&
          (first_address_function_code == Default::ANALOG_INPUT_REGISTERS_FUNCTION_CODE ||
           first_address_function_code == Default::ANALOG_OUTPUT_HOLDING_REGISTERS_FUNCTION_CODE)) ||
         (polling_group.second[i] - last_register == 8 &&
          (first_address_function_code == Default::DISCRETE_OUTPUT_COILS_FUNCTION_CODE ||
           first_address_function_code == Default::DISCRETE_INPUT_CONTACTS_FUNCTION_CODE))) {
        sequence_group.emplace_back(polling_group.second[i]);
      } else {
        if(last_register == 0) {
          sequence_group.emplace_back(polling_group.second[i]);
        } else {
          address_sequences.emplace_back(sequence_group);
          sequence_group.clear();
          sequence_group.emplace_back(polling_group.second[i]);
        }
      }

      if(i == polling_group.second.size() - 1) {
        address_sequences.emplace_back(sequence_group);
      } else {
        last_register = polling_group.second[i];
      }
    }

    polling_groups.emplace(polling_group.first, address_sequences);
  }
}

bool Modbus::on_configuration_change(const ConfigurationInterface& config) {
  // Remove all the old registrations
  unregister_callbacks(properties.registrations);
  properties.registrations.clear();

  bool ret = true;

  // RTU configurations
  if(!ConfigurationInterface::has_key(config, "minus_offset")) {
    LOG_WARN("%s", "# Modbus WARNING # Missing Configuration key: minus_offset\n");
  }

  minus_offset = config["minus_offset"].as<uint16_t>(1);
  debug = config["debug"].as<bool>(false);
  rtu_timestamp = config["rtu_timestamp"].as<bool>(false);
  chunk_size = config["chunk_size"].as<uint16_t>(120);
  timestamp_sync = config["timestamp_sync"].as<bool>(false);
  connection_initialized = false;

  if(timestamp_sync) {
    LOG_INFO("%s", "# Modbus # Timestamp Sync ON\n");
  }

  if(ConfigurationInterface::has_key(config, "dynacard_validation")) {
    dynacard_validation_enabled = true;
    status_fault_address = config["dynacard_validation"]["status_fault_address"].as<std::int32_t>();
    status_mode_address = config["dynacard_validation"]["status_mode_address"].as<std::int32_t>();
    status_mode_aml = config["dynacard_validation"]["status_mode_aml"].as<std::vector<uint16_t>>();
    config_card_source_address = config["dynacard_validation"]["config_card_source_address"].as<std::int32_t>();
    status_card_source_address = config["dynacard_validation"]["status_card_source_address"].as<std::int32_t>();
    card_source_value = config["dynacard_validation"]["card_source_value"].as<std::uint16_t>();
    status_card_confirmation = config["dynacard_validation"]["status_card_confirmation"].as<bool>();
  }

  LOG_DEBUG("# Modbus # chunk_size: %d\n", chunk_size);

  if(ConfigurationInterface::has_key(config, "connection")) {
    connection = config["connection"].as<ConfigurationInterface>();
  } else {
    LOG_ERROR("%s", "# Modbus ERROR # Missing Configuration key: configuration: connection\n");

    ret = false;
  }

  build_raw_polling_groups();

  /// Dynagraph Polling
  if(!raw_dynagraph_polling_groups.empty()) {
    for(const auto& dynagraph_polling_group : raw_dynagraph_polling_groups) {
      const auto dynagraph_polling_period_nano =
          std::chrono::nanoseconds(static_cast<uint64_t>(dynagraph_polling_group.first * 1.0E9));

      for(const auto& dynagraph_info : dynagraph_polling_group.second) {
        properties.registrations.emplace_back(
            register_callback(std::make_unique<descriptor::Poller>(dynagraph_polling_period_nano),
                              [this, dynagraph_info] { read_dynagraph_data(dynagraph_info); }));
      }
    }
  } else {
    LOG_WARN("%s", "# Modbus WARNING # There are no dynagraph polling groups\n");
  }

  /// Polling
  if(!raw_polling_groups.empty()) {
    group_polling_addresses();

    /// Register a Poller callback for each register sequence by period
    for(const auto& polling_group : polling_groups) {
      const auto polling_group_adresses = polling_group.second;
      const auto polling_period = polling_group.first;
      const auto polling_period_nano = std::chrono::nanoseconds(static_cast<uint64_t>(polling_period * 1.0E9));

      properties.registrations.emplace_back(register_callback(
          std::make_unique<descriptor::Poller>(polling_period_nano),
          [this, polling_period, polling_group_adresses] { poll_data(polling_period, polling_group_adresses); }));
    }
  } else {
    LOG_WARN("%s", "# Modbus WARNING # There are no polling groups\n");
  }

  return ret;
}

void Modbus::on_terminate() {
  /// Close Modbus connection
  disconnect();

  modbus_connection = nullptr;
  registers_mapping.clear();
  polling_groups.clear();
  raw_polling_groups.clear();
  raw_dynagraph_polling_groups.clear();

  LOG_INFO("%s", "# Modbus # Terminated\n");
}

}  // namespace Modbus

std::unique_ptr<ApplicationInterface> new_instance() {
  return std::make_unique<Modbus::Modbus>();
}
