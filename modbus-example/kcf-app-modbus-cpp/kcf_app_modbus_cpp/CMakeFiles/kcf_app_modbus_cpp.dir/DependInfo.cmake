# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/opt/kelvin/app/kcf_app_modbus_cpp/kcf_app_modbus_cpp.cpp" "/opt/kelvin/app/kcf_app_modbus_cpp/CMakeFiles/kcf_app_modbus_cpp.dir/kcf_app_modbus_cpp.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/opt/kelvin/datamodel/include"
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
