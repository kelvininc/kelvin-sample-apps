{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Process Plant Example\n",
    "This example will demonstrate building a <a href=\"https://documentation.kelvininc.com/sdk/apps/intro/\">Kelvin SDK (KSDK)</a> application (app) to communicate to a plant process via OPC-UA. After the app is validated and tested locally, it can be deployed to an <a href=\"https://documentation.kelvininc.com/platform/control_center/acps/\">ACP</a> for controlling real hardware.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Contents\n",
    "1. Narrative\n",
    "2. Getting Started\n",
    "3. Building Kelvin SDK Applications\n",
    "4. App Emulation\n",
    "5. Interrogating OPC-UA Data\n",
    "6. Simulator UI\n",
    "7. Utilizing Local OPC-UA History\n",
    "8. Shutting Down Apps\n",
    "9. Next Steps\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Narrative\n",
    "The demonstration process will simulate the inflow and outflow of a holding vessel (H1 ). The Control Valve V1 opens based upon the downstream demand of the process.  A Pump (P1) maintains level in Vessel V1 by varying the inflow to the vessel based upon the demand caused by the open position of Valve (V1) (monitoring the level via the Level transmitter LT1).\n",
    "\n",
    "![PNID](images/ProcessPlantPNIDmed.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Problem\n",
    "\n",
    "The Holding Vessel Process was conceived with an anticipated inflow and outflow range. Due to external conditions the pump P1 must adapt to the demand from the outlet valve V1. The example in this notebook utilizes simple pump start and stop setpoints, the idea is to demonstrate control, then move on to a more complex control algorithm later.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Getting Started\n",
    "It's a good idea to start by reading the Kelvin documentation https://documentation.kelvininc.com/ to understand the <a href=\"https://documentation.kelvininc.com/\">Kelvin Platform</a>, <a href=\"https://documentation.kelvininc.com/sdk/release_notes/\">SDK</a>, and working in the <a href=\"https://documentation.kelvininc.com/sandbox/overview/\">Kelvin Sandbox</a>. \n",
    "\n",
    "This notebook will start by going over how the Process Plant Control application is configured, then move on to running and validating the app. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Verify KSDK version\n",
    "This usecase requires KSDK version 3.0.0 or greater."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk --version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Upgrading KSDK\n",
    "If the version is below 4.0.0 you will need to perform a manual upgrade of your environment KSDK version."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "username=input()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import getpass\n",
    "userpassword = getpass.getpass()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! pip install --index-url https://{username}:{userpassword}@nexus.kelvininc.com/repository/pypi-kelvin/simple --upgrade kelvin-sdk==4.2.0\n",
    "userpassword = ''"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have special characters in your password you must escape them and use the ASCII equivalent as this is a URL.\n",
    "\n",
    "For instance ```password1!``` must be ```password1%21```\n",
    "\n",
    "https://www.w3schools.com/tags/ref_urlencode.ASP\n"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "|Char|ASCII|\n",
    "|----|-----|\n",
    "| !  | %21 |\n",
    "| @  | %40 |\n",
    "| #  | %23 |\n",
    "| $  | %24 |\n",
    "| %  | %25 |\n",
    "| ^  | %5E |\n",
    "| &  | %26 |\n",
    "| *  | %2A |\n",
    "| (  | %28 |\n",
    "| )  | %29 |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Logging Into Kelvin Platform\n",
    "To work with the KSDK you must be logged into a Kelvin Platform. \n",
    "Either log in from a terminal window or use the cells below.\n",
    "\n",
    "```ksdk login <yourinstance>.kelvininc.com ```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk --current-session"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "username=input()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import getpass\n",
    "userpassword = getpass.getpass()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Log into your KSDK server, you can do this from the terminal\n",
    "! ksdk auth login titan.kelvininc.com --username {username} --password {userpassword}\n",
    "#userpassword = ''"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. Building Kelvin SDK Applications\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating our Application\n",
    "The process-plant-control Kelvin SDK app will connect to the PLC/Simulator, perform logic, then write control changes back out to the PLC/Simulator.\n",
    "Creating apps is easy, simply type ```ksdk app create <appname>``` more information on creating apps can be found <a href=\"https://docs.kelvininc.com/sdk/apps/managing_local_apps/create_apps/\">here</a>."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build Apps\n",
    "Applications are built using the ```ksdk app build``` command. Append ```--verbose``` to see more detailed build output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Build the control application\n",
    "! ksdk app build --app-dir=process-plant-control"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Build the plant simulator (the first build will take some time and have a lot of output!)\n",
    "! cd kcf-app-modbus-cpp && ksdk app build --verbose"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. App Emulation\n",
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Emulation\n",
    "<a href=\"https://docs.kelvininc.com/sdk/apps/emulating_apps/prepare_an_app_for_emulation/\">KSDK emulation</a> enables running apps immediately for testing. ```ksdk emulation start``` will start the app found in the immediate directory of the command prompt or Jupyter notebook. You may also start apps from anywhere by name, with ```ksdk emulation start <app-image-name:version>```. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Start the Modbus poller application\n",
    "! cd kcf-app-modbus-cpp && ksdk emulation start --port-mapping 48010:48011"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Start the control application\n",
    "! (cd process-plant-control && ksdk emulation start --port-mapping 48010:48010)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Images and Containers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Check kelvin app images and running applications, note, the simulator is not a KSDK app but a simple docker container\n",
    "! ksdk app images list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Standard Docker commands work as well, this shows running containers\n",
    "! docker ps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the logs of the polling app\n",
    "# This may work better in a terminal\n",
    "! ksdk emulation logs kcf-app-modbus-cpp:0.0.3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# View the logs of the application for any errors, press the stop icon above to interrupt the log\n",
    "# This may work better in a terminal window\n",
    "! ksdk emulation logs process-plant-control:0.0.4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5. Interrogating OPC-UA Data\n",
    "## OPC-UA Commander\n",
    "With OPC-UA Commander you can view the OPC-UA data in your app to verify its operation. \n",
    "Use the arrow keys to navigate into the Objects group.\n",
    "Press M when a tag is highlighted to monitor its live values. Then press Q to quit the program.\n",
    "\n",
    "![OPC-UA Commander](images/opcuacommander.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You can inspect the OPCUA server with an OPCUA client such as OPCUA Commander, this command will install it\n",
    "# https://github.com/node-opcua/opcua-commander\n",
    "# You only need to install the OPCUA Commander once after your VM is created\n",
    "! npm -g install opcua-commander xml-writer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note, OPCUA Commander needs to be executed in a terminal window \n",
    "opcua-commander -e opc.tcp://$(docker port process-plant-control 48010)\n",
    "opcua-commander -e opc.tcp://localhost:53880    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 6. Simulator UI\n",
    "### Node-RED User Interface\n",
    "The Node-RED simulator provides a built-in web user interface to view the simulators live data.\n",
    "\n",
    "If developing on a local machine simply navigate with a web browser to http://localhost:1880/ui"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Launch the Node-RED dashboard user interface webpage from within the Kelvin Sandbox\n",
    "# you may need to modify the URL sub domain name\n",
    "import os, re\n",
    "from IPython.display import HTML\n",
    "user_name = os.environ[\"JUPYTERHUB_USER\"]\n",
    "height = 620\n",
    "#url = f\"https://sandbox.kelvininc.com/user/{user_name}/proxy/1880/ui/\"\n",
    "url = f\"https://sandbox.kelvininc.com/user/{user_name}/proxy/1880/ui/\"\n",
    "print(url)\n",
    "display(HTML(f\"\"\"<iframe src=\"{url}\" width=\"1000px\" height=\"{height}px\"/>\"\"\" ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may modify the pump speed and valve position setpoints as well as switch the valves into manual/auto modes.\n",
    "\n",
    "When the pump is set to manual, control is coming from the KSDK app to control the pump speed.\n",
    "\n",
    "![PNID](images/ProcessPlantPNIDmed.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 7. Utilizing Local OPC-UA History\n",
    "Kelvin applications use OPC-UA for default between app communications. You can utilize python OPC-UA libraries such as <a href=\"https://github.com/FreeOpcUa/opcua-asyncio\">opcua-asyncio</a> to collect live data and history from any OPC-UA server. We will also use <a href=\"https://pandas.pydata.org/\">pandas</a> to manipulate our data and <a href=\"https://matplotlib.org/\">matplotlib</a> to display a plot in the Jupyter Notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! pip install asyncua"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Import libraries into the notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import asyncua\n",
    "import pandas\n",
    "from datetime import datetime, timedelta, timezone\n",
    "import matplotlib.pyplot as pyplot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instantiate an asyncua client, pull OPC-UA history and prepare it"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ua_client = asyncua.Client(\"opc.tcp://localhost:48010\")\n",
    "await ua_client.connect()\n",
    "\n",
    "now = datetime.now(tz=timezone.utc)\n",
    "then = now - timedelta(minutes=5)\n",
    "\n",
    "node = ua_client.get_node(\"ns=2;s=tank_level\")\n",
    "data = await node.read_raw_history(starttime=then, endtime=now, numvalues=10000)\n",
    "def convert(x):\n",
    "    return {**{k: v for k, v in vars(x).items() if not k.startswith(\"_\")}, \"Value\": x.Value.Value}\n",
    "data = pandas.DataFrame([convert(v) for v in data])\n",
    "data = data.drop(columns=['Encoding', 'SourcePicoseconds', 'ServerPicoseconds', 'ServerTimestamp', 'StatusCode'])\n",
    "tankLevel = data.rename(columns={'Value':'TankLevel', 'SourceTimestamp':'Timestamp'})\n",
    "\n",
    "node2 = ua_client.get_node(\"ns=2;s=valve_position\")\n",
    "data = await node2.read_raw_history(starttime=then, endtime=now, numvalues=10000)\n",
    "def convert(x):\n",
    "    return {**{k: v for k, v in vars(x).items() if not k.startswith(\"_\")}, \"Value\": x.Value.Value}\n",
    "data = pandas.DataFrame([convert(v) for v in data])\n",
    "data = data.drop(columns=['Encoding', 'SourcePicoseconds', 'ServerPicoseconds', 'ServerTimestamp', 'StatusCode'])\n",
    "valvePosition = data.rename(columns={'Value':'ValvePosition', 'SourceTimestamp':'vts'})\n",
    "\n",
    "node2 = ua_client.get_node(\"ns=2;s=speed_setpoint\")\n",
    "data = await node2.read_raw_history(starttime=then, endtime=now, numvalues=10000)\n",
    "def convert(x):\n",
    "    return {**{k: v for k, v in vars(x).items() if not k.startswith(\"_\")}, \"Value\": x.Value.Value}\n",
    "data = pandas.DataFrame([convert(v) for v in data])\n",
    "data = data.drop(columns=['Encoding', 'SourcePicoseconds', 'ServerPicoseconds', 'ServerTimestamp', 'StatusCode'])\n",
    "speedSetpoint = data.rename(columns={'Value':'SpeedSetpoint', 'SourceTimestamp':'sts'})\n",
    "\n",
    "result = pandas.concat([tankLevel, valvePosition, speedSetpoint], axis=1, join='inner')\n",
    "df = result.drop(columns=['vts', 'sts'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##### Plot the results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyplot.figure(figsize=(15,8)) \n",
    "pyplot.plot(df.Timestamp, df.TankLevel, label=\"Tank Level\" )\n",
    "pyplot.plot(df.Timestamp, df.ValvePosition, label=\"Outlet Valve Position\" )\n",
    "pyplot.plot(df.Timestamp, df.SpeedSetpoint, label=\"Pump Speed\" )\n",
    "pyplot.legend(bbox_to_anchor=(0, .09, .3, 1), loc='upper left', ncol=2, mode=\"sdf\", borderaxespad=0.)\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 8. Shutting Down Apps\n",
    "The typical way to stop an application you are working on is to type ```ksdk emulation stop``` from the terminal while in the app directory. \n",
    "\n",
    "To stop apps from anywhere add the ```<app-name:version>``` argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! ksdk emulation stop process-plant-control:0.0.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```ksdk emulation stop``` may delete the docker 'ksdk' network which may in turn terminate the simulator, if not, you can stop it with the ```docker stop``` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! docker stop process-plant-simulator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 9. Next Steps\n",
    "Since app names uploaded to the Kelvin <a href=\"https://documentation.kelvininc.com/platform/control_center/app_registry/\">App Registry</a> must be unique, you can't upload and deploy this \"process-plant-control\" application as is. The following Jupyter Notebooks in this module cover the steps of creating your own app like the one we have just tested, deploying it to the app registry, and further learning on how to consume data from the cloud historian. \n",
    "\n",
    "Open up the next notebook to create an application from scratch.\n",
    "\n",
    "[2-Building-From-Scratch.ipynb](2-Building-From-Scratch.ipynb) - Build the Process Plant Usecase from scratch learning what it takes to create a <a href=\"https://documentation.kelvininc.com/sdk/apps/intro/\">Kelvin SDK</a> application."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6-final"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}