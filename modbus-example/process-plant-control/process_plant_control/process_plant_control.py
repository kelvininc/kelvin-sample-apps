"""
Process Plant Control application.
"""

from kelvin.app import ApplicationConfig, DataApplication
from typing import Any

class AppConfig(ApplicationConfig):
    """Application Config."""

class App(DataApplication):
    """Application."""

    config: AppConfig  

    def process(self) -> None:
        """Process data."""
        lower_setpoint = self.config.thresholds.vessel_low_setpoint.value
        upper_setpoint = self.config.thresholds.vessel_high_setpoint.value
        vessel_level = self.data.vessel_level.value

        if vessel_level <= lower_setpoint or vessel_level >= upper_setpoint:
            if vessel_level < lower_setpoint:
                out_speed_setpoint = 60
            else :
                out_speed_setpoint = 0
            print(f'Changing speed output to {out_speed_setpoint}')
            sp = self.make_message(
                "raw.float32",
                "speed_setpoint",
                value = out_speed_setpoint
            )
        else:
            sp = self.make_message(
                "raw.float32",
                "speed_setpoint",
                value = self.data.pump_speed_setpoint.value
            )

        vpos = self.make_message(
            "raw.float32",
            "valve_position",
            time_of_validity = sp._.time_of_validity,
            value = self.data.valve_position_feedback.value
        )
        tl = self.make_message(
            "raw.float32",
            "tank_level",
            time_of_validity = sp._.time_of_validity,
            value = vessel_level 
        )
        self.emit(sp)        
        self.emit(vpos)          
        self.emit(tl) 
        print(F'tank_level= {tl.value}   speed_setpoint= {sp.value}   valve_position= {vpos.value}')

