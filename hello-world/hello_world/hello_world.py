'''
This app starts up, sets initialized point true, then emits a random number each second
'''
# python program library imports
from kelvin.app import DataApplication
import numpy as np

class App(DataApplication):

    def on_initialize(self, configuration: dict) -> bool:
        # on_initialized runs once on startup
        # self.message emits data to outputs
        self.message(name="initialized", type="raw.int32", value=1, emit=True)
        # you can print to the log with print() or self.logger.info() 
        self.logger.info("Starting Up...")
        return True

    def process(self):
        # process() executes each period defined in the app.yaml
        # generate a random number using the numpy.random.randn() function
        random_num = np.random.randn(1)
        # write the output named "output"
        self.message(name="output", type="raw.float32", value=random_num, emit=True)
        # print the random number to the console log 
        self.logger.info(f"Random number: {random_num}")
