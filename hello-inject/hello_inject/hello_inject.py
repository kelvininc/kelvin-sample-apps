"""
Kelvin Hello Inject application based on "kelvin.app" which takes a temperature input 
in Celsius and converts it to Fahrenheit.

"""

from kelvin.app import ApplicationConfig, DataApplication


class AppConfig(ApplicationConfig):
    """Application Config."""
    # This is where you can set up static parameters among other things
    #lower: float = 0.0
    #upper: float = 1000.0


class App(DataApplication):
    """Application."""

    config: AppConfig
    # Set up how kelvin.app processes incoming data
    TOPICS = {
        "#.*": {
            "target": "history.{name}",
            "storage_type": "buffer",
            "storage_config": {"window": {"minutes": 10}, "getter": "value"},
        }
    }

    def process(self) -> None:
        """Process data."""
        # Get the temperature in celsius
        temp_c = self.data.temperature_c.value
        print(temp_c)
        # Convert to Fahrenheit
        degf = ((temp_c * (9/5)) + 32)

        # Set up some messages to output
        f = self.make_message(
            "raw.float32",
            "temperature_f",
            value= degf
        )
        c = self.make_message(
            "raw.float32",
            "temperature_c_out",
            time_of_validity=f._.time_of_validity,
            value = temp_c
        )

        # Emit the data
        self.emit(f)
        self.emit(c)
