{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# KSDK 2.0 Hello World Injection Example"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example will demonstrate building a Kelvin SDK (KSDK) hello world application (app) that utilizes the Kelvin data injector to simulate live data. \n",
    "\n",
    "The main python program ```hello_inject.py``` can be found in the ```hello_inject``` sub directory.\n",
    "\n",
    "The data injector will provide Temperature in Celsius data to the app, the app will convert the Temperature to Fahrenheit, then emit the output to the OPC-UA Data Buss. The data injector will grab values from a CSV file found in the \"data\" folder / \"data.csv\" file and inject it into the program for simulation and testing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Contents\n",
    "1. Getting Started - Kelvin SDK documentation and help\n",
    "\n",
    "2. Understanding the Hello Inject App\n",
    "\n",
    "    2.1 hello_world.py\n",
    "   \n",
    "    2.2 app.yaml\n",
    "    \n",
    "    2.3 environment.yaml\n",
    "    \n",
    "3. Build, Run, Test\n",
    "\n",
    "    3.1 Logging In\n",
    "    \n",
    "    3.2 Building Apps\n",
    "    \n",
    "    3.3 Running Apps\n",
    "    \n",
    "    3.4 Data Injector\n",
    "    \n",
    "    3.5 View Images and Containers\n",
    "    \n",
    "    3.6 View Logs\n",
    "    \n",
    "4. Interrogating OPC-UA Data\n",
    "\n",
    "5. Visualize Data\n",
    "\n",
    "    5.1 Extract OPC-UA Data with Python\n",
    "    \n",
    "    5.2 Plot Data with Matplotlib\n",
    "\n",
    "6. Shutting Down Apps\n",
    "\n",
    "7. Next Steps\n",
    "\n",
    "    7.1 Creating apps from scratch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Getting Started\n",
    "It's a good idea to start by reading the Kelvin documentation https://documentation.kelvininc.com/. In this notebook we start by going over how the Hello World app is configured, then we will the run and validate the app. \n",
    "\n",
    "Another way to get help when working with the Kelvin SDK (KSDK) is to use the ```--help``` flag to investigate commands. Below are some examples of using the built in SDK documentation. \n",
    "\n",
    "If you are new to Jupyter notebooks, they are a convenient way to document and visualize code. Highlight a cell below, then press Shift + Enter or click the \"Run Selected Cells\" button above to view the command line output. If the notebook gets too cluttered with output, you may right click and select \"Clear All Outputs\". "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! ksdk --version"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk --help"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk auth login --help"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk app --help"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk app build --help"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk emulation start --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Understanding the Hello Inject App\n",
    "### 2.1 hello_inject.py\n",
    "The following code snippet from hello_inject.py performs the temperature conversion from Celsius to Fahrenheit in the \"on_data\" method. The app prints the resulting values to the log then emits the value back to the OPC-UA data buss. It also emits the original temperature to ```temperature_c_out```, this is to make sure the data is alligned in our chart later on.\n",
    "\n",
    "```python\n",
    "    def on_data(self, input_data) -> None:\n",
    "        \"\"\"\n",
    "        The callback that is triggered when there is data available for the model to process.\n",
    "        The incoming data will be single data model objects.\n",
    "\n",
    "        Parameters:\n",
    "        data (MessageInterface): the data being supplied to the executing model.\n",
    "            The MessageInterface type can be set to a derived type like Float, PlungerLift, etc\n",
    "            in you application by specifying that type in the method declaration.\n",
    "        \"\"\"\n",
    "        for item in input_data:\n",
    "            degf = ((item.value * (9/5)) + 32)\n",
    "\n",
    "            message = Float32('temperature_f')\n",
    "            message.value = degf\n",
    "            print(item.name, item.value, message.name, message.value)\n",
    "            self.emit(message)\n",
    "\n",
    "            message = Float32('temperature_c_out')\n",
    "            message.value = item.value\n",
    "            self.emit(message)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! cat hello_world/hello_world.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 app.yaml\n",
    "The app.yaml file defines what version of KSDK and base image to use when building the app, it's also where you define app inputs and outputs. There is one input ```temperature_c```, and two outputs ```temperature_f``` and ```temperature_c_out```. All data values are defined as being  Float32 datatypes.\n",
    "```yaml\n",
    "inputs:\n",
    "  temperature_c:\n",
    "    type: raw.float32\n",
    "\n",
    "outputs:\n",
    "  temperature_f:\n",
    "    type: raw.float32\n",
    "  temperature_c_out:\n",
    "    type: raw.float32\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! cat app.yaml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 environment.yaml\n",
    "The environment.yaml file defines input and output bindings for the app's OPC-UA server's tags.\n",
    "\n",
    "```yaml\n",
    "bindings:\n",
    "\n",
    "  inputs:\n",
    "    - endpoint: opc.tcp://hello-inject-input:48010\n",
    "      source: ns=2;s=temperature_c\n",
    "      target: temperature_c\n",
    "\n",
    "  outputs:\n",
    "    - endpoint: opc.tcp://hello-inject:48010\n",
    "      source: temperature_f\n",
    "      target: ns=2;s=temperature_f\n",
    "    - endpoint: opc.tcp://hello-inject:48010\n",
    "      source: temperature_c_out\n",
    "      target: ns=2;s=temperature_c_out \n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! cat environment.yaml"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.1 Build, Run, Test\n",
    "### 3.1 Logging Into Kelvin Platform\n",
    "To work with the KSDK you must be logged into a Kelvin Platform to pull and push app images. This must be done from a terminal window.\n",
    "```ksdk login <yourinstance>.kelvininc.com ```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Log into your KSDK server, do this from the terminal\n",
    "ksdk auth login demo.kelvininc.com"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.2 Building Apps\n",
    "Applications are built using the ```ksdk app build``` command. Append ```--verbose``` to see more detailed build output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Build the application\n",
    "! ksdk app build"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.3 Starting Apps\n",
    "The KSDK emulation system enables running apps immediately for testing. ```ksdk emulation start``` will start the app found in the immediate directory of the command prompt or Jupyter notebook. You may also start apps from anywhere by name, with ```ksdk emulation start <app-image-name:version>```.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Start the application\n",
    "! ksdk emulation start --port-mapping 48010:48010"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.4 Data Injector \n",
    "The KSDK data injector app does not need to be built. Pass in the ```--app-name``` and file path arguments when calling the app.\n",
    "\n",
    "Arguments:\n",
    "- ```--app-name``` - name of the app to inject data into\n",
    "- ```data/data.CSV``` - , the path to the file\n",
    "- ```--period``` - optional, the frequency to inject in seconds\n",
    "- ```--repeat``` - optional, reload the file and keep injecting forever"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Start the data injector\n",
    "! ksdk data inject --app-name hello-inject:0.0.1 --period 1 data/data.csv --repeat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.5 View Images and Containers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check kelvin app images and running applications\n",
    "! ksdk app images list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Standard Docker commands work as well, this shows running containers\n",
    "! docker ps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3.6 View Logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# View the logs of the application for any errors, press the stop icon above to interrupt the log\n",
    "# This may work better in a terminal window\n",
    "! ksdk emulation logs hello-inject:0.0.1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You can also check the logs of the data injector, press the stop icon above to interrupt the log\n",
    "# This may work better in a terminal\n",
    "! ksdk emulation logs hello-inject-input:latest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Interrogating OPC-UA Data\n",
    "With OPC-UA Commander you can view the OPC-UA data in your app to verify its operation. \n",
    "Use the arrow keys to navigate into the Objects group.\n",
    "Press M when a tag is highlighted to monitor its live values. Then press Q to quit the program.\n",
    "\n",
    "![OPC-UA Commander](images/opcuacommander.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You can inspect the OPCUA server with an OPCUA client such as OPCUA Commander, this command will install it\n",
    "# https://github.com/node-opcua/opcua-commander\n",
    "# You only need to install the OPCUA Commander once after your VM is created\n",
    "! npm -g install opcua-commander xml-writer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Note, OPCUA Commander needs to be executed in a terminal window \n",
    "opcua-commander -e opc.tcp://localhost:48010"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Visualize Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.1 Extract OPC-UA Data with Python\n",
    "AsyncUA is a Python OPC-UA client that can be used to collect data, save it to a .csv file, then chart it with pyplot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# opcua-asyncio is an open source Python OPC-UA library https://github.com/FreeOpcUa/opcua-asyncio, this command will install it\n",
    "# You only need to install opcua-asyncio once after your VM is created\n",
    "! pip install asyncua"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! pip install matplotlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import Python libraries\n",
    "from asyncua import Client\n",
    "import asyncio\n",
    "import pandas as pd\n",
    "import datetime\n",
    "import matplotlib.pyplot as pyplot\n",
    "import matplotlib.dates as mdates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Delete the results.csv file if it exists with del in Windows and rm in Linux\n",
    "! del data\\results.csv rem\n",
    "#! rm data/results.csv"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a fresh CSV file\n",
    "name_dict = {'temperature_c': [], 'temperature_f': [], 'indx': []}\n",
    "df = pd.DataFrame(name_dict)       \n",
    "df.to_csv('data/results.csv', index=False) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Connect to the application OPC-UA buss, collect data and append it to the ```results.csv``` file. This code will loop 10 times appending the records to the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "alldf = pd.DataFrame()\n",
    "async with Client(url='opc.tcp://127.0.0.1:48010') as client:\n",
    "    for i in range(10):\n",
    "        # Do something with client\n",
    "        degc = client.get_node('ns=2;s=temperature_c_out')\n",
    "        temperature_c = await degc.read_value()\n",
    "        degf = client.get_node('ns=2;s=temperature_f')\n",
    "        temperature_f = round(await degf.read_value(),1)\n",
    "\n",
    "        print(\"temperature_c \" + str(temperature_c) + \"     temperature_f \" + str(temperature_f))  \n",
    "\n",
    "        # create pandas data frame and write to CSV\n",
    "        name_dict = {'temperature_c': [temperature_c], 'temperature_f': [temperature_f], 'indx': [i]}\n",
    "        df = pd.DataFrame(name_dict)       \n",
    "        df.to_csv('data/results.csv', mode='a', header=False, index=False) \n",
    "        alldf = alldf.append(df)\n",
    "                \n",
    "        await asyncio.sleep(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5.2 Plot Data with Matplotlib\n",
    "Load the CSV file into a Pandas DataFrame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv('data/results.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Show the dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyplot.figure(figsize=(15,10)) \n",
    "pyplot.plot(df.indx, df.temperature_c, label=\"Celsius\" )\n",
    "pyplot.plot(df.indx, df.temperature_f, label=\"Fahrenheit\" )\n",
    "pyplot.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',\n",
    "           ncol=2, mode=\"expand\", borderaxespad=0.)\n",
    "pyplot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Shutting Down Apps\n",
    "The typical way to stop an application you are working on is to type ```ksdk emulation stop``` while in the app directory. To stop other apps add the ```<app-name:version>``` argument."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! ksdk emulation stop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! ksdk emulation stop hello-inject-input:latest"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "! docker ps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 7. Next steps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Edit the hello_inject/hello_inject.py program and make some changes, rebuild build and deploy the app. \n",
    "- Edit the yaml configuration files to add additional inputs and outputs.\n",
    "\n",
    "### 7.1 Building Apps from Scratch\n",
    "To build a new app from scratch use ```ksdk app create <app-name>```, you can configure the new app to run similarly to the original Hello World app example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! ksdk app create hello-inject-2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6-final"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}